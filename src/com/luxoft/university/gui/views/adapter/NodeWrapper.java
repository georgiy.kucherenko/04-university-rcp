package com.luxoft.university.gui.views.adapter;

import com.luxoft.university.api.models.*;

/**
 * This class extends com.luxoft.university.api.models.BaseNode.
 * Instance of this class is a wrapping object for tree that is used in 
 * University App.
 */
public class NodeWrapper extends BaseNode {
	private static final long serialVersionUID = 1L;
	private GroupNode root = null;

	@Override
	public String getId() {
		return null;
	}

	@Override
	public String getName() {
		return null;
	}

	@Override
	public GroupNode getParent() {
		return null;
	}

	@Override
	public GroupNode getRoot() {
		return root;
	}

	@Override
	protected void setParent(GroupNode parent) {
	}
	
	/**
	 * Method is used for adapterFactory. 
	 * 
	 * @return empty array if root is null. Otherwise returns array with one element (root)
	 */
	public BaseNode[] getEntries() {
		if (root != null) {
			return new BaseNode[] {root};
		}
		return new BaseNode[] {};
	}

	/**
	 * Public Setter 
	 * 
	 * @param root sets root element to be wrapped
	 */
	public void setRoot(GroupNode root) {
		this.root = root;
	}
}
