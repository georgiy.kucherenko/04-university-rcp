package com.luxoft.university.gui.views.adapter;

import org.eclipse.core.runtime.IAdapterFactory;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.ui.model.IWorkbenchAdapter;
import org.eclipse.ui.plugin.AbstractUIPlugin;

import com.luxoft.university.Application;
import com.luxoft.university.api.models.*;
import com.luxoft.university.gui.utils.ImageConstants;


/**
 * This class extends org.eclipse.core.runtime.IAdapterFactory. 
 */
public class AdapterFactory implements IAdapterFactory {
	private final NodeWrapper wrapper;
	
	/**
	 * Public constructor
	 * 
	 * @param wrapper - wrapper for rootNode
	 */
	public AdapterFactory(NodeWrapper wrapper) {
		this.wrapper = wrapper;
	}
	
	private IWorkbenchAdapter rootAdapter = new IWorkbenchAdapter() {
		
		@Override
		public Object getParent(Object o) {
			return null;
		}
		
		@Override
		public String getLabel(Object o) {
			return "";
		}

		@Override
		public ImageDescriptor getImageDescriptor(Object object) {
			return null;
		}

		@Override
		public Object[] getChildren(Object o) {
			return ((NodeWrapper) o).getEntries();
		}
	};

	private IWorkbenchAdapter groupAdapter = new IWorkbenchAdapter() {
		
		@Override
		public Object getParent(Object o) {
			if (((GroupNode) o).getParent() == null) {
				return wrapper;
			}
			return ((GroupNode) o).getParent();
		}
		
		@Override
		public String getLabel(Object o) {
			return ((GroupNode) o).getName();
		}

		@Override
		public ImageDescriptor getImageDescriptor(Object object) {
			return AbstractUIPlugin.imageDescriptorFromPlugin(
					Application.PLUGIN_ID, ImageConstants.GROUP_NODE.getFilePath());
		}

		@Override
		public Object[] getChildren(Object o) {
			return ((GroupNode) o).getEntries();
		}
	};

	private IWorkbenchAdapter personAdapter = new IWorkbenchAdapter() {
		
		@Override
		public Object getParent(Object o) {
			return ((PersonNode) o).getParent();
		}
		
		@Override
		public String getLabel(Object o) {
			PersonNode entry = ((PersonNode) o);
			return entry.getName() ;
		}
		
		@Override
		public ImageDescriptor getImageDescriptor(Object object) {
			return AbstractUIPlugin.imageDescriptorFromPlugin(
					Application.PLUGIN_ID, ImageConstants.PERSON_NODE.getFilePath());
		}

		@Override
		public Object[] getChildren(Object o) {
			if (o instanceof GroupNode) {
				return ((GroupNode) o).getEntries();
			} else {
				return new Object[0];
			}
		}
	};

	@Override
	public Object getAdapter(Object adaptableObject, Class adapterType) {
		if (adapterType == IWorkbenchAdapter.class && adaptableObject instanceof GroupNode) {
			return groupAdapter;
		}
		if (adapterType == IWorkbenchAdapter.class && adaptableObject instanceof PersonNode) {
			return personAdapter;
		}
		if (adapterType == IWorkbenchAdapter.class && adaptableObject instanceof NodeWrapper) {
			return rootAdapter;
		}
		
		return null;
	}

	@Override
	public Class[] getAdapterList() {
		return new Class[] { IWorkbenchAdapter.class };
	}
}