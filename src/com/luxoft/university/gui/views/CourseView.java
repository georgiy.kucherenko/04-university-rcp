package com.luxoft.university.gui.views;

import static com.luxoft.university.gui.actions.settings.ActionUtility.getActionById;
import static org.eclipse.core.runtime.Platform.getAdapterManager;
import static com.luxoft.university.gui.utils.LogConstants.*;

import org.eclipse.core.runtime.*;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.viewers.*;
import org.eclipse.swt.SWT;
import org.eclipse.swt.dnd.*;
import org.eclipse.swt.widgets.*;
import org.eclipse.ui.model.*;
import org.eclipse.ui.part.*;

import com.luxoft.university.api.UniverService;
import com.luxoft.university.api.data_managers.FileDataManager;
import com.luxoft.university.api.models.*;
import com.luxoft.university.gui.actions.*;
import com.luxoft.university.gui.editors.*;
import com.luxoft.university.gui.listener.*;
import com.luxoft.university.gui.views.adapter.*;
import com.luxoft.universitylogger.api.*;
/**
 * This class extends org.eclipse.ui.part.ViewPart. It is used to display the
 * model of University App.
 */
public class CourseView extends ViewPart {
	public static final String ID = "com.luxoft.university.views.courseview";
	private NodeWrapper wrapper = new NodeWrapper();
	private TreeViewer treeViewer;
	private UniverService service;
	private ListenerList<CourseViewListener> listeners;

	@Override
	public void setFocus() {
		treeViewer.getControl().setFocus();
		fireEvent(EventTypes.UPDATE_SAVE_ENABLE_STATUS);
	}

	@Override
	public void createPartControl(Composite parent) {
		init();
		setupTreeViewer(parent);
		addCourseViewListeners();
		tuneDragAndDrop();
		setupPopups();
	}

	/**
	 * Public getter for UniverServise
	 * 
	 * @return UniverService instance
	 */
	public UniverService getService() {
		return service;
	}

	/**
	 * Public setter for setting the model
	 * 
	 * @param studyGroup - the model to be set
	 */
	public void setViewerInput(GroupNode studyGroup) {
		treeViewer.setInput(studyGroup);
	}

	/**
	 * Public method for refreshing the treeViewer
	 */
	public void refreshViewer() {
		treeViewer.refresh();
	}

	/**
	 * Public method for getting the selection from treeViewer
	 * 
	 * @return current selection of treeViewer
	 */
	public TreeSelection getTreeSelection() {
		return (TreeSelection) treeViewer.getSelection();
	}

	/**
	 * Method adds CourseViewListener to listeners list
	 * 
	 * @param listener to be added
	 */
	public void addCourseViewListener(CourseViewListener listener) {
		if (listeners == null) {
			listeners = new ListenerList<CourseViewListener>();
		}
		listeners.add(listener);
	}

	/**
	 * Method removes CourseViewListener from listeners list
	 * 
	 * @param listener - listener to be removed
	 */
	public void removeCouseViewListener(CourseViewListener listener) {
		if (listeners == null) {
			return;
		}
		
		listeners.remove(listener);
		
		if (listeners.isEmpty()) {
			listeners = null;
		}
	}

	/**
	 * FireEvent-method This method notifies all listeners about fired event
	 * 
	 * @param event - event to be send to all listeners
	 */
	public void fireEvent(EventTypes event) {
		if (listeners == null) {
			return;
		}

		for (CourseViewListener courseViewListener : listeners) {
			courseViewListener.handleEvent(event);
		}
	}

	/**
	 * Method returns root element of treeViewer
	 * 
	 * @return root node
	 */
	public BaseNode getRoot() {
		return wrapper.getRoot();
	}
	
	/**
	 * Method sets root node to treeViewer
	 * 
	 * @param groupNode to be set
	 */
	public void setRoot(GroupNode groupNode) {
		wrapper.setRoot(groupNode);
		LOGGER.info(NEW_ROOT_NODE_IS_SET);
	}

	/**
	 * Method expands elements of treeViewer
	 */
	public void expandTree() {
		treeViewer.expandAll();
	}

	private void tuneDragAndDrop() {
		addViewToEditorDND();
		addViewToViewDND();
	}

	private void setupTreeViewer(Composite parent) {
		treeViewer = new TreeViewer(parent, SWT.BORDER | SWT.MULTI | SWT.V_SCROLL);
		
		getAdapterManager().registerAdapters(new AdapterFactory(wrapper), BaseNode.class);
		getSite().setSelectionProvider(treeViewer);
		treeViewer.setLabelProvider(new WorkbenchLabelProvider());
		treeViewer.setContentProvider(new BaseWorkbenchContentProvider());
		treeViewer.addDoubleClickListener(createDoubleClickListener());
		treeViewer.addSelectionChangedListener(event -> fireEvent(EventTypes.UPDATE_SAVE_ENABLE_STATUS));
		treeViewer.setInput(wrapper);
	}

	private void addCourseViewListeners() {
		addCourseViewListener((CourseViewListener) (getActionById(SaveEntityAction.ID).get()));
		addCourseViewListener((CourseViewListener) (getActionById(SaveAllEntitiesAction.ID).get()));
		addCourseViewListener((CourseViewListener) (getActionById(SaveFileAction.ID).get()));
		addCourseViewListener((CourseViewListener) (getActionById(NewGroupAction.ID).get()));
		addCourseViewListener((CourseViewListener) (getActionById(NewPersonAction.ID).get()));
	}

	private void setupPopups() {
		MenuManager menuMgr = createContextMenuManager();
		Menu menu = menuMgr.createContextMenu(treeViewer.getControl());

		treeViewer.getControl().setMenu(menu);
		getSite().registerContextMenu(menuMgr, treeViewer);
	}

	private MenuManager createContextMenuManager() {
		MenuManager menuMgr = new MenuManager(POPUPS);
		 
		menuMgr.add(createPopupNew());
		menuMgr.add(createPopupSave());
		menuMgr.add(getActionById(DeleteEntityAction.ID).get());
		return menuMgr;
	}

	private MenuManager createPopupSave() {
		MenuManager saveInstance = new MenuManager(SAVE_SECTION);
		saveInstance.add(getActionById(SaveEntityAction.ID).get());
		saveInstance.add(getActionById(SaveAllEntitiesAction.ID).get());
		return saveInstance;
	}

	private MenuManager createPopupNew() {
		MenuManager newInstance = new MenuManager(NEW_SECTION);
		newInstance.add(getActionById(NewGroupAction.ID).get());
		newInstance.add(getActionById(NewPersonAction.ID).get());
		return newInstance;
	}
	
	private IDoubleClickListener createDoubleClickListener() {
		return ((event) -> getActionById(OpenAction.ID).get().run());
	}

	private void init() {
		service = new UniverService(new FileDataManager());
		setPartProperty(PROP_EDITED, PROP_EDITED_FALSE);
	}

	private void addViewToViewDND() {
		int operations = DND.DROP_MOVE | DND.DROP_COPY | DND.DROP_LINK;
		Transfer[] transferTypes = new Transfer[] {TextTransfer.getInstance()};
		
		DropTarget target = new DropTarget(treeViewer.getControl(), operations);
		target.setTransfer(transferTypes);
		
		target.addDropListener (new ViewerDropTargetAdapter(treeViewer));
	}

	private void addViewToEditorDND() {
		int editorOperations = DND.DROP_COPY | DND.DROP_MOVE;
		Transfer[] transferTypes = new Transfer[] { EditorInputTransfer.getInstance(), TextTransfer.getInstance() };

		DragSourceListener listener = createDragSourceListener();
		treeViewer.addDragSupport(editorOperations, transferTypes, listener);
	}

	private DragSourceListener createDragSourceListener() {
		return new DragSourceAdapter() {

			@Override
			public void dragSetData(DragSourceEvent event) {
				if (EditorInputTransfer.getInstance().isSupportedType(event.dataType)) {
					
					EditorInputTransfer.EditorInputData input = EditorInputTransfer
							.createEditorInputData(CourseEditor.ID, new CourseEditorInput(getSelection()));
					
					event.data = new EditorInputTransfer.EditorInputData[] {input};
					
					return;
					
				} else if (TextTransfer.getInstance().isSupportedType(event.dataType)) {
					event.data = getSelection().getId();
					
					return;
				}
				
				event.doit = false;
			}

			private BaseNode getSelection() {
				return (BaseNode)getTreeSelection().getFirstElement();
			}
		};
	}
	

	public static final String PROP_EDITED = "edited";
	public static final String PROP_EDITED_TRUE = "true";
	public static final String PROP_EDITED_FALSE = "false";
	private static final String POPUPS = "Group popup";
	private static final String NEW_SECTION = "New..:";
	private static final String SAVE_SECTION = "Save..:";
	private static ILogger LOGGER = LoggerFactory.getLogger(CourseView.class);
}
