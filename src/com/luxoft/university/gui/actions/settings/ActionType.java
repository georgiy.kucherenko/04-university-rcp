package com.luxoft.university.gui.actions.settings;

import org.eclipse.swt.SWT;


/**
 * Enum for storing Actions' properties 
 */
public enum ActionType {
	OPEN_File ("Open File..", SWT.CTRL +  'O', "icons/open_file.png"),
	SAVE_FILE ("Save File..", SWT.CTRL + SWT.SHIFT + SWT.ALT + 'S', "icons/save.png"),
	NEW_FILE ("New StudyGroup..", SWT.CTRL + SWT.SHIFT + 'N', "icons/new_file.png"),
	OPEN_NODE ("Open Group/Person", SWT.CTRL + SWT.ALT + 'O', "icons/open_file.png"),
	NEW_GROUP ("Create New Group", SWT.CTRL + SWT.ALT + 'M', "icons/new_folder.png"),
	NEW_PERSON ("Create New Person", SWT.CTRL + SWT.ALT + 'N', "icons/new_person.png"),
	SAVE_NODE ("Save Group or Person", SWT.CTRL + SWT.ALT + 'S', "icons/save.png"),
	SAVE_ALL_NODES ("Save all Groups and Persons", -1, "icons/save_all.png"),
	DELETE_NODE ("Delete Group or Person", SWT.CTRL + SWT.ALT + 'D', "icons/delete.png"), 
	ABOUT ("About..", -1, "icons/university16.png");

	private String description;
	private int accelerator;
	private String iconPath;

	ActionType(String description, int accelerator, String iconPath) {
		this.description = description;
		this.accelerator = accelerator;
		this.iconPath = iconPath;
	}

	/**
	 * Getter for description
	 * 
	 * @return - enum's description value
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Getter for Accelerator
	 * 
	 * @return - enum's Accelerator value
	 */
	public int getAccelerator() {
		return accelerator;
	}

	/**
	 * Getter for IconPath
	 * 
	 * @return - enum's iconPath value
	 */
	public String getIconPath() {
		return iconPath;
	}
}