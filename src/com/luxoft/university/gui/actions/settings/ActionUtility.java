package com.luxoft.university.gui.actions.settings;

import java.util.*;
import java.util.List;

import org.eclipse.jface.action.*;
import org.eclipse.swt.widgets.*;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.plugin.AbstractUIPlugin;

import com.luxoft.university.Application;


/**
 * Utility class for tuning actions during their creation, and getting action by id
 */
public final class ActionUtility {

	private ActionUtility() {
	}
	
	/**
	 * Method is tuning action - setting all necessary properties
	 * 
	 * @param id - String value to be set as id
	 * @param actionProps - enum Instance that contains settings
	 * @param action - action Instance to be tuned
	 */
	public static void tuneAction(String id, ActionType actionProps, Action action) {
		action.setId(id);
		action.setText(actionProps.getDescription());
		action.setToolTipText(actionProps.getDescription());
		if (actionProps.getAccelerator() != -1) {
			action.setAccelerator(actionProps.getAccelerator());
		}
		action.setImageDescriptor(
				AbstractUIPlugin.imageDescriptorFromPlugin(Application.PLUGIN_ID, actionProps.getIconPath()));
	}
	
	/**
	 * Method finds Univesity's App action by it's ID
	 * 
	 * @param id - id of searched action
	 * @return Optional<IAction> - optional found action
	 */
	public static Optional<IAction> getActionById(String id) {
		MenuItem[] menuItems = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell().getMenuBar().getItems();
		return Arrays.stream(menuItems)
				.map(ActionUtility::mapMenuItemToContributionList)
				.flatMap(contributionList -> contributionList.stream())
				.filter(contribution -> filterContributionById(contribution, id))
				.map(contribution -> ((ActionContributionItem) contribution).getAction())
				.findFirst();
	}

	private static boolean filterContributionById(IContributionItem contribution, String id) {
		String contributionId = contribution.getId();
		return contributionId != null && contributionId.equals(id);
	}
	
	private static List<IContributionItem> mapMenuItemToContributionList(MenuItem menuItem){
		return Arrays.asList((((MenuManager) menuItem.getData()).getItems()));
	}
}
