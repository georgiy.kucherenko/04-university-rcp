package com.luxoft.university.gui.actions;

import static com.luxoft.university.api.utils.NodeUtilities.getAllChilds;
import static com.luxoft.university.gui.actions.settings.ActionUtility.tuneAction;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.viewers.*;
import org.eclipse.ui.*;
import org.eclipse.ui.actions.ActionFactory.IWorkbenchAction;

import com.luxoft.university.api.models.BaseNode;
import com.luxoft.university.gui.actions.settings.ActionType;


/**
 * This action is dedicated to deleting entities from model in University App.
 */
public class DeleteEntityAction extends Action implements IWorkbenchAction, ISelectionListener {
	public final static String ID = "com.luxoft.university.actions.deleteentityaction";
	private final ContextAccesser accesser;

	/**
	 * Public constructor
	 * 
	 * @param accesser - IWorkbenchWindow wrapper for accessing context
	 * @param actionType - instance holding properties for this action
	 */
	public DeleteEntityAction(ContextAccesser accesser, ActionType actionType) {
		this.accesser = accesser;
		tuneAction(ID, actionType, this);
		accesser.addListenerToSelectionService(this);
		setEnabled(false);
	}


	@Override
	public void dispose() {
	}

	@Override
	public void run() {
		BaseNode baseNode = accesser.getNodeFromTreeSelection();
		baseNode.detachFromParent();
		closeNodeAndChilds(baseNode);
		accesser.refreshViewer();
	}
	
	@Override
	public void selectionChanged(IWorkbenchPart part, ISelection selection) {
		BaseNode node = accesser.getNodeFromTreeSelection();

		setEnabled(!(node == null || node.getParent() == null));
	}

	private void closeNodeAndChilds(BaseNode baseNode) {
		getAllChilds(baseNode).stream()
		.forEach(accesser::ensureClosing);

		accesser.ensureClosing(baseNode);
	}
}
