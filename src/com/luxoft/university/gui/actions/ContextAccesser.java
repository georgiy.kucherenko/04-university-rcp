package com.luxoft.university.gui.actions;

import java.util.*;

import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.jface.viewers.TreeSelection;
import org.eclipse.ui.*;
import org.eclipse.ui.part.*;

import com.luxoft.university.api.exceptions.*;
import com.luxoft.university.api.models.*;
import com.luxoft.university.gui.editors.*;
import com.luxoft.university.gui.listener.*;
import com.luxoft.university.gui.utils.*;
import com.luxoft.university.gui.views.CourseView;
import com.luxoft.universitylogger.api.*;

import static com.luxoft.university.gui.utils.LogConstants.*;

/**
 * Wrapping class that wraps IWorkBenchWindow and provides different delegating methods. 
 * Also provides methods for processing the data
 */
public class ContextAccesser { 
	private static ILogger LOGGER = LoggerFactory.getLogger(CourseView.class);
	private final IWorkbenchWindow window;

	/**
	 * Public constructor
	 * 
	 * @param window - IWorkbenchWindow instance to be encapsulated
	 */
	public ContextAccesser(IWorkbenchWindow window) {
		this.window = window;
	}
	
	
	GroupNode getParentForNewNode() {
		CourseView courseView = getCourseView();
		BaseNode currentSelection = (BaseNode) getTreeSelection().getFirstElement();

		return currentSelection == null ?
				(GroupNode) courseView.getRoot() : 
					(currentSelection instanceof GroupNode ?
							(GroupNode) currentSelection :
								((PersonNode) currentSelection).getParent());
	}
	
	TreeSelection getTreeSelection() {
		return  getCourseView().getTreeSelection();
	}
	
	IWorkbenchPage getCurrentPage() {
		return window.getActivePage();
	}
	
	IEditorPart openEditor(IEditorInput input, String editorId) throws PartInitException {
		return getCurrentPage().openEditor(input, editorId);
	}

	void doSaveActiveEditor() {
		getActiveEditor().doSave(new NullProgressMonitor());
	}
	
	void refreshViewer() {
		getCourseView().refreshViewer();
	}

	BaseNode getNodeFromTreeSelection() {
		return (BaseNode)(getTreeSelection().getFirstElement());
	}

	void ensureClosing(BaseNode input) {
		IWorkbenchPage currentPage = getCurrentPage();
		
		Arrays.stream(currentPage.getEditorReferences())
		.map(ref -> ref.getEditor(false))
		.filter(editor -> verifyEditorsInput(input, editor))
		.findAny()
		.ifPresent(editor -> currentPage.closeEditor(editor, false));
	}

	void addListenerToSelectionService(ISelectionListener action) {
		window.getSelectionService().addSelectionListener(action);
	}

	boolean verifyActiveIsDirty() {
		return getActiveEditor() == null ?
				false : getActiveEditor().isDirty();
	}
 
	void fireView(EventTypes eventType) {
		getCourseView().fireEvent(eventType);
	}

	boolean verifyDirtiesExist() {
		return getDirtyEditorsList().size() != 0;
	}
	
	boolean validateActiveEditor() {
		return ((CourseEditor) getActiveEditor()).performValidation();
	}
	
	List<? extends IEditorPart> getDirtyEditorsList() {
		return Arrays.asList(getCurrentPage().getDirtyEditors());
	}

	List<CourseEditor> getNotValidatedEditors() {
		return getDirtyEditorsList().stream()
				.map(editor -> (CourseEditor)editor)
				.filter(editor -> !editor.performValidation())
				.toList();
	}

	void closeAllEditors(boolean shouldSave) {
		getCurrentPage().closeAllEditors(shouldSave);
	}

	String getViewProperty(String propEdited) {
		return getCourseView().getPartProperty(propEdited);
	}

	void setViewProperty(String propEdited, String propEditedFalse) {
		getCourseView().setPartProperty(propEdited, propEditedFalse);
	}

	void saveModelToFile(String nameAndPath) throws DataManagerException {
		getCourseView().getService().saveData(nameAndPath, (GroupNode)getCourseView().getRoot());
	}

	void loadAndSetModel(String fullPath) throws DataManagerException {
		setState(getCourseView().getService().loadData(fullPath));
	}

	void setStateToNull() {
		setState(null);
	}

	void setBlankModel() {
		setState(getCourseView().getService().createRoot());
	}

	boolean queryModelIsNull() {
		return getCourseView().getRoot() == null;
	}
	
	boolean verifyFocusOnEditor() {
		if (getCurrentPage().getActivePart() instanceof EditorPart) {
			return true;
		}

		return false;
	}
	
	boolean verifyFocusOnView() {
		if (getCurrentPage().getActivePart() instanceof ViewPart) {
			return true;
		}
		
		return false;
	}

	boolean verifySelectionIsEdited() {
		 BaseNode selectedNode = getNodeFromTreeSelection();
		 
		 if (selectedNode == null) {
			return false;
		 }
		
		 String id = selectedNode.getId();
		 
		return getDirtyEditorsList().stream()
				.filter(editor -> id.equals(getNodeFromEditor(editor).getId()))
				.findAny().isPresent();
	}

	void openSelectionInEditor() {
		String id = getNodeFromTreeSelection().getId();
		
		CourseEditorInput input = getDirtyEditorsList().stream()
				.filter(a -> id.equals(getNodeFromEditor(a).getId()))
				.map(a -> ((CourseEditorInput)((CourseEditor)a).getEditorInput()))
				.findAny().get();
		
		try {
			openEditor(input, CourseEditor.ID);
		} catch (PartInitException e) {
			LOGGER.error(ERROR_OPENING_EDITOR, e);
			new ExceptionNotifier(e);
		}
	}

	void setFocusOnView() {
		getCourseView().setFocus();
	}
	private void setState(GroupNode groupNode) {
		CourseView courseView = getCourseView();
		
		closeAllEditors(false);
		courseView.setRoot(groupNode);
		courseView.setPartProperty(CourseView.PROP_EDITED, CourseView.PROP_EDITED_FALSE);
		courseView.fireEvent(EventTypes.MODEL_PRESENCE_UPDATED);
		
		refreshViewer();
		courseView.expandTree();
	}

	private IEditorPart getActiveEditor() {
		return getCurrentPage().getActiveEditor();
	}
	
	private CourseView getCourseView() {
		return (CourseView)(getCurrentPage().findView(CourseView.ID));
	}

	private boolean verifyEditorsInput(BaseNode input, IEditorPart editor) {
		return input.getId()
				.equals(((CourseEditorInput) editor.getEditorInput()).getNode().getId());
	}

	private BaseNode getNodeFromEditor(IEditorPart editor) {
		return (BaseNode)((CourseEditorInput)((CourseEditor)editor).getEditorInput()).getNode();
	}
}