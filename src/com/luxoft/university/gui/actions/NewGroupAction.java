package com.luxoft.university.gui.actions;

import static com.luxoft.university.gui.actions.settings.ActionUtility.tuneAction;

import org.eclipse.jface.action.Action;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.actions.ActionFactory.IWorkbenchAction;

import com.luxoft.university.api.models.GroupNode;
import com.luxoft.university.api.utils.NodeUtilities;
import com.luxoft.university.gui.actions.settings.ActionType;
import com.luxoft.university.gui.editors.*;
import com.luxoft.university.gui.listener.*;
import com.luxoft.university.gui.utils.ExceptionNotifier;


/**
 * This action is dedicated to creating new GroupNode in model of University App.
 */
public class NewGroupAction extends Action implements IWorkbenchAction, CourseViewListener {
	public final static String ID = "com.luxoft.university.actions.newgroupaction";
	private final ContextAccesser accesser;

	/**
	 * Public constructor
	 * 
	 * @param accesser - IWorkbenchWindow wrapper for accessing context
	 * @param actionType - instance holding properties for this action
	 */
	public NewGroupAction(ContextAccesser accesser, ActionType actionType) {
		this.accesser = accesser;
		tuneAction(ID, actionType, this);
		setEnabled(false);
	}

	
	@Override
	public void dispose() {
	}

	@Override
	public void run() {
		GroupNode parent = accesser.getParentForNewNode();
		GroupNode studyGroup = NodeUtilities.createBlankGroup(parent);

		try {
			accesser.openEditor(new CourseEditorInput(studyGroup), CourseEditor.ID);
		} catch (PartInitException e) {
			new ExceptionNotifier(e);
		}
	}

	@Override
	public void handleEvent(EventTypes eventType) {
		if (eventType == EventTypes.MODEL_PRESENCE_UPDATED) {
			setEnabled(!accesser.queryModelIsNull());
		}
	}
}