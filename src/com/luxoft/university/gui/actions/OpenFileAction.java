package com.luxoft.university.gui.actions;

import static com.luxoft.university.gui.actions.settings.ActionUtility.tuneAction;
import static com.luxoft.university.gui.utils.StoreStateUtility.*;
import static com.luxoft.university.gui.utils.LogConstants.*;

import org.eclipse.jface.action.Action;
import org.eclipse.swt.SWT;
import org.eclipse.ui.actions.ActionFactory.IWorkbenchAction;

import com.luxoft.university.api.exceptions.DataManagerException;
import com.luxoft.university.gui.actions.settings.ActionType;
import com.luxoft.university.gui.utils.*;
import com.luxoft.university.gui.utils.FileDialogWrapper.WrapperType;
import com.luxoft.universitylogger.api.*;

/**
 * This action is dedicated to opening source file with model in University App.
 */
public class OpenFileAction extends Action implements IWorkbenchAction {
	public static final String ID = "com.luxoft.university.actions.openfileaction";
	private static ILogger LOGGER = LoggerFactory.getLogger(OpenFileAction.class);
	private final ContextAccesser accesser;

	/**
	 * Public constructor
	 * 
	 * @param accesser - IWorkbenchWindow wrapper for accessing context
	 * @param actionType - instance holding properties for this action
	 */
	public OpenFileAction(ContextAccesser accesser, ActionType actionType) {
		this.accesser = accesser;
		tuneAction(ID, actionType, this);
	}

	@Override
	public void dispose() {
	}

	@Override
	public void run() {
		performOpenFile();
	}

	private void performOpenFile() {
		if (!passStoreControl()) {
			return;
		}
		accesser.setStateToNull();
		
		FileDialogWrapper dlg = new FileDialogWrapper(WrapperType.COURSE_FILES, SWT.OPEN);
		
		if (dlg.open() == null) {
			return;
		}

		try {
			accesser.loadAndSetModel(dlg.getFullPath());
			LOGGER.info(SUCCESSFUL_OPENING_FILE + dlg.getFullPath());
		} catch (DataManagerException exception) { 
			new ExceptionNotifier(exception);
			LOGGER.error(FAIL_OPENING_FILE + dlg.getFullPath(), exception);
		}
	}
}
