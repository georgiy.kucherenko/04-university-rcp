package com.luxoft.university.gui.actions;

import static com.luxoft.university.gui.actions.settings.ActionUtility.*;
import static com.luxoft.university.gui.utils.YesNoCancelDialog.*;
import static com.luxoft.university.gui.views.CourseView.*;
import static com.luxoft.university.gui.utils.LogConstants.*;

import org.eclipse.jface.action.Action;
import org.eclipse.swt.SWT;
import org.eclipse.ui.actions.ActionFactory.IWorkbenchAction;

import com.luxoft.university.api.exceptions.DataManagerException;
import com.luxoft.university.gui.actions.settings.ActionType;
import com.luxoft.university.gui.listener.*;
import com.luxoft.university.gui.utils.*;
import com.luxoft.university.gui.utils.FileDialogWrapper.WrapperType;
import com.luxoft.universitylogger.api.*;

/**
 * This action is dedicated to saving the model to a file in University App.
 */
public class SaveFileAction extends Action implements IWorkbenchAction,  CourseViewListener{
	public static final String ID = "com.luxoft.university.actions.savefileaction";
	private static ILogger LOGGER = LoggerFactory.getLogger(SaveFileAction.class);
	private static final String TITLE = "Save changes?";
	private static final String QUESTION = "There are unsaved records.. should we save them?";
	private final ContextAccesser accesser;

	/**
	 * Public constructor
	 * 
	 * @param accesser - IWorkbenchWindow wrapper for accessing context
	 * @param actionType - instance holding properties for this action
	 */
	public SaveFileAction(ContextAccesser accesser, ActionType actionType) {
		this.accesser = accesser;
		tuneAction(ID, actionType, this);
		setEnabled(false);
	}

	@Override
	public void dispose() {
	}

	@Override
	public void run() {
		if (accesser.verifyDirtiesExist()) {
			int dialogResult = new YesNoCancelDialog(TITLE, QUESTION).open();
			
			if (dialogResult == YES) {
				getActionById(SaveAllEntitiesAction.ID).get().run();
				
				if (accesser.verifyDirtiesExist()) {
					return;
				}
				
			} else if (dialogResult == NO) {
				accesser.closeAllEditors(false);
			} else { 
				return;
			}
		}
		
		performSaveFile();
	}

	@Override
	public void handleEvent(EventTypes eventType) {
		String edited = accesser.getViewProperty(PROP_EDITED);
		if (PROP_EDITED_TRUE.equals(edited)) {
			setEnabled(true);
		} else if (PROP_EDITED_FALSE.equals(edited)) {
			setEnabled(false);
		}
	}
	
	private void performSaveFile() {
		FileDialogWrapper dlg = new FileDialogWrapper(WrapperType.COURSE_FILES, SWT.SAVE);
		
		if (dlg.open() == null) {
			return;
		}

		try {
			accesser.saveModelToFile(dlg.getFullPath());
			LOGGER.info(SUCCESSFUL_SAVING_FILE + dlg.getFullPath());
		} catch (DataManagerException exception) {
			new ExceptionNotifier(exception);
			LOGGER.error(FAIL_SAVING_FILE + dlg.getFullPath(), exception);
		}
		accesser.setViewProperty(PROP_EDITED, PROP_EDITED_FALSE);
		setEnabled(false);
	}
}
