package com.luxoft.university.gui.actions;

import static com.luxoft.university.gui.actions.settings.ActionUtility.tuneAction;
import static com.luxoft.university.gui.utils.StoreStateUtility.*;

import org.eclipse.jface.action.Action;
import org.eclipse.ui.actions.ActionFactory.IWorkbenchAction;

import com.luxoft.university.gui.actions.settings.ActionType;


/**
 * This action is dedicated to creating new model for University App.
 */
public class NewFileAction extends Action implements IWorkbenchAction {
	public final static String ID = "com.luxoft.university.actions.newfileaction";
	private final ContextAccesser accesser;

	/**
	 * Public constructor
	 * 
	 * @param accesser - IWorkbenchWindow wrapper for accessing context
	 * @param actionType - instance holding properties for this action
	 */
	public NewFileAction(ContextAccesser accesser, ActionType actionType) {
		this.accesser = accesser;
		tuneAction(ID, actionType, this);
	}

	@Override
	public void dispose() {
	}

	@Override
	public void run() {
		if (!passStoreControl()) {
			return;
		}
		accesser.setStateToNull();
		
		performNewFile();
	}

	private void performNewFile() {
		accesser.setBlankModel();
	}
}