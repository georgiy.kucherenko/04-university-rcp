package com.luxoft.university.gui.actions;

import static com.luxoft.university.gui.actions.settings.ActionUtility.tuneAction;
import static com.luxoft.university.gui.utils.LogConstants.*;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.viewers.*;
import org.eclipse.ui.*;
import org.eclipse.ui.actions.ActionFactory.IWorkbenchAction;

import com.luxoft.university.api.models.BaseNode;
import com.luxoft.university.gui.actions.settings.ActionType;
import com.luxoft.university.gui.editors.*;
import com.luxoft.university.gui.utils.ExceptionNotifier;
import com.luxoft.universitylogger.api.*;


/**
 * This action is dedicated to opening the node of model of University App.
 */
public class OpenAction extends Action implements IWorkbenchAction, ISelectionListener {
	public final static String ID = "com.luxoft.university.actions.openaction";
	private static ILogger LOGGER = LoggerFactory.getLogger(OpenAction.class);
	private final ContextAccesser accesser;

	/**
	 * Public constructor
	 * 
	 * @param accesser - IWorkbenchWindow wrapper for accessing context
	 * @param actionType - instance holding properties for this action
	 */
	public OpenAction(ContextAccesser accesser, ActionType actionType) {
		this.accesser = accesser;
		tuneAction(ID, actionType, this);
		accesser.addListenerToSelectionService(this);
		setEnabled(false);
	}


	@Override
	public void dispose() {
	}
	
	@Override
	public void run() {
		BaseNode studyItem = accesser.getNodeFromTreeSelection();
		
		if (studyItem.getParent() == null) {
			return;
		}
		
		try {
			accesser.openEditor(new CourseEditorInput(studyItem), CourseEditor.ID);
			LOGGER.info(OPENED_EDITOR + studyItem.toString());
		} catch (PartInitException e) {
			LOGGER.error(ERROR_OPENING_EDITOR, e);
			new ExceptionNotifier(e);
		}
	}

	@Override
	public void selectionChanged(IWorkbenchPart part, ISelection selection) {
		BaseNode node = accesser.getNodeFromTreeSelection();

		setEnabled(!(node == null || node.getParent() == null));
	}
}