package com.luxoft.university.gui.actions;

import static com.luxoft.university.gui.actions.settings.ActionUtility.tuneAction;
import static com.luxoft.university.gui.listener.EventTypes.*;

import java.util.*;

import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.jface.action.Action;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.actions.ActionFactory.IWorkbenchAction;

import com.luxoft.university.gui.actions.settings.ActionType;
import com.luxoft.university.gui.editors.*;
import com.luxoft.university.gui.listener.*;
import com.luxoft.university.gui.utils.*;


/**
 * This action is dedicated to operation of "Save All opened editors" of University App.
 */
public class SaveAllEntitiesAction extends Action implements IWorkbenchAction, CourseViewListener {
	public final static String ID = "com.luxoft.university.actions.saveallentitiesaction";
	private static final String ERROR_BLANK_MESSAGE = "Cannot save following:\n\n";
	private static final String GAP = "\n";
	private final ContextAccesser accesser;

	/**
	 * Public constructor
	 * 
	 * @param accesser - IWorkbenchWindow wrapper for accessing context
	 * @param actionType - instance holding properties for this action
	 */
	public SaveAllEntitiesAction(ContextAccesser accesser, ActionType actionType) {
		this.accesser = accesser;
		tuneAction(ID, actionType, this);
		setEnabled(false);
	}


	@Override
	public void dispose() {
	}
	
	@Override
	public void run() {
		List<CourseEditor> notValidated = accesser.getNotValidatedEditors();
		
		if (notValidated.size() > 0) {
			notifyUser(notValidated);
			return;
		}
		
		PriorityQueue<IEditorPart> queue = new PriorityQueue<>(createIEditorPartComparator());
		queue.addAll(accesser.getDirtyEditorsList());

		while (queue.peek() != null) { 
			queue.poll().doSave(new NullProgressMonitor());
		}
		
		accesser.refreshViewer();
		accesser.fireView(UPDATE_SAVE_ENABLE_STATUS);
	}

	@Override
	public void handleEvent(EventTypes type) {
		if (type == UPDATE_SAVE_ALL_ENABLE_STATUS || type == UPDATE_SAVE_ENABLE_STATUS) {
			setEnabled(accesser.verifyDirtiesExist());
		} 
	}
	
	private void notifyUser(List<CourseEditor> editors) {
		String errorMessage = editors.stream()
		.map(this::mapEditorToString)
		.reduce(ERROR_BLANK_MESSAGE, (result, node) -> result + node + GAP);
		
		new ExceptionNotifier(errorMessage);
	}
	
	private Comparator<IEditorPart> createIEditorPartComparator(){
		return (o1, o2) -> 
			((CourseEditorInput) o2.getEditorInput()).getNode().getDepth()
					- ((CourseEditorInput) o1.getEditorInput()).getNode().getDepth();
	}
	
	private String mapEditorToString(CourseEditor editor) {
		return ((CourseEditorInput) editor.getEditorInput()).getNode().toString(); 
	}
}
