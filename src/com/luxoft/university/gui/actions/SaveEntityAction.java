package com.luxoft.university.gui.actions;

import static com.luxoft.university.gui.actions.settings.ActionUtility.tuneAction;
import static com.luxoft.university.gui.utils.LogConstants.*;

import org.eclipse.jface.action.Action;
import org.eclipse.ui.actions.ActionFactory.IWorkbenchAction;

import com.luxoft.university.gui.actions.settings.ActionType;
import com.luxoft.university.gui.listener.*;
import com.luxoft.university.gui.utils.*;
import com.luxoft.universitylogger.api.*;


/**
 * This action is dedicated to saving the node of model of University App.
 */
public class SaveEntityAction extends Action implements IWorkbenchAction, CourseViewListener {
	public final static String ID = "com.luxoft.university.actions.saveentityaction";
	private final static String FAIL_VALIDATION_MESSAGE = "Name and Group must not be empty!";
	private static ILogger LOGGER = LoggerFactory.getLogger(SaveEntityAction.class);
	private final ContextAccesser accesser;

	/**
	 * Public constructor
	 * 
	 * @param accesser - IWorkbenchWindow wrapper for accessing context
	 * @param actionType - instance holding properties for this action
	 */
	public SaveEntityAction(ContextAccesser accesser, ActionType actionType) {
		this.accesser = accesser;
		tuneAction(ID, actionType, this);
		setEnabled(false);
	}

	
	@Override
	public void dispose() {
	}
	
	@Override
	public void run() {
		if (!accesser.verifyFocusOnEditor()) {
			accesser.openSelectionInEditor();
		}
		
		if(!(accesser.validateActiveEditor())) {
			new ExceptionNotifier(FAIL_VALIDATION_MESSAGE);
			LOGGER.info(FAIL_VALIDATION_LOG);
			return;
		}
		
		accesser.doSaveActiveEditor();
		setEnabled(false);
		accesser.setFocusOnView();
		accesser.refreshViewer();
		accesser.fireView(EventTypes.UPDATE_SAVE_ALL_ENABLE_STATUS);
	}

	@Override
	public void handleEvent(EventTypes eventType) {
		if (eventType != EventTypes.UPDATE_SAVE_ENABLE_STATUS) {
			return;
		}
		
		if (accesser.verifyFocusOnEditor()) {
			setEnabled(accesser.verifyActiveIsDirty());
		} else if (accesser.verifyFocusOnView()) {
			setEnabled(accesser.verifySelectionIsEdited());
		} else {
			setEnabled(false);
		}
	}
}
