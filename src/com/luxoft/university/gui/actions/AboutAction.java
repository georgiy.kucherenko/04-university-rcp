package com.luxoft.university.gui.actions;

import static com.luxoft.university.gui.actions.settings.ActionUtility.tuneAction;

import org.eclipse.jface.action.Action;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.actions.ActionFactory.IWorkbenchAction;

import com.luxoft.university.gui.actions.dialog.AboutDialog;
import com.luxoft.university.gui.actions.settings.ActionType;


/**
 * This action is dedicated to showing About dialog of University App.
 */
public class AboutAction extends Action implements IWorkbenchAction {
	public final static String ID = "com.luxoft.university.actions.aboutaction";


	/**
	 * Public constructor
	 * 
	 * @param actionType - instance holding properties for this action
	 */
	public AboutAction(ActionType actionType) {
		tuneAction(ID, actionType, this);
	}


	@Override
	public void dispose() {
	}

	@Override
	public void run() {
		new AboutDialog(Display.getCurrent().getActiveShell()).open();
	}
}

