package com.luxoft.university.gui.actions.dialog;

import static com.luxoft.university.gui.utils.ImageScaler.scaleImage;
import static org.eclipse.ui.plugin.AbstractUIPlugin.imageDescriptorFromPlugin;

import org.eclipse.jface.dialogs.*;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.*;
import org.eclipse.swt.widgets.*;

import com.luxoft.university.Application;

/**
 * This class extends org.eclipse.jface.dialogs.IconAndMessageDialog. 
 * It provides an AboutDialog for University App.
 */
public class AboutDialog extends IconAndMessageDialog {
	private static final String DEFAULT_AVA_LOCATION = "icons/ava_gang.png";
	private static final String DIALOG_MESSAGE = "University App\n(c) 2023";
	private static final int MAX_HEIGHT = 400;
	private static final int MAX_WIDTH = 100;
	private Image image;

	/**
	 * Public constructor
	 * 
	 * @param parent - active Shell 
	 */
	public AboutDialog(Shell parent) {
		super(parent);
		image = createDialogImage();
		message = DIALOG_MESSAGE;
	}

	@Override
	public boolean close() {
		if (image != null) {
			image.dispose();
		}
		return super.close();
	}

	@Override
	protected Control createDialogArea(Composite parent) {
		createMessageArea(parent);

		Composite composite = new Composite(parent, SWT.NONE);
		GridData data = new GridData(GridData.FILL_BOTH);
		data.horizontalSpan = 2;
		composite.setLayoutData(data);
		composite.setLayout(new FillLayout());
		return composite;
	}

	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		createButton(parent, IDialogConstants.OK_ID, IDialogConstants.OK_LABEL, true);
	}

	@Override
	protected void buttonPressed(int buttonId) {
		close();
	}

	@Override
	protected Image getImage() {
		return image;
	}

	private Image createDialogImage() {
		return scaleImage(
				getParentShell().getDisplay(), 
				imageDescriptorFromPlugin(Application.PLUGIN_ID, DEFAULT_AVA_LOCATION), 
				MAX_WIDTH, 
				MAX_HEIGHT
				);
	}
}