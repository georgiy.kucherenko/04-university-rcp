package com.luxoft.university.gui.app_config;

import org.eclipse.swt.graphics.Point;
import org.eclipse.ui.application.*;
import org.eclipse.ui.part.EditorInputTransfer;

import static com.luxoft.university.gui.utils.StoreStateUtility.*;

/**
 * This class extends org.eclipse.ui.application.WorkbenchWindowAdvisor.  
 * It is public base class for configuring a workbench window 
 */
public class ApplicationWorkbenchWindowAdvisor extends WorkbenchWindowAdvisor {
	private static final String UNIVERSITY_APP = "University App";

	/**
     * Public constructor
     * 
     * @param configurer - interface providing special access for configuring workbench windows
     */
	public ApplicationWorkbenchWindowAdvisor(IWorkbenchWindowConfigurer configurer) {
        super(configurer);
    }
    
	
    @Override
    public ActionBarAdvisor createActionBarAdvisor(IActionBarConfigurer configurer) {
        return new ApplicationActionBarAdvisor(configurer);
    }
    
    @Override
    public void preWindowOpen() {
        IWorkbenchWindowConfigurer configurer = getWindowConfigurer();
        configurer.setInitialSize(new Point(800, 430));
        configurer.setShowCoolBar(true);
        configurer.setShowStatusLine(true);
        configurer.setShowMenuBar(true);
        configurer.setTitle(UNIVERSITY_APP); //$NON-NLS-1$
        configurer.addEditorAreaTransfer(EditorInputTransfer.getInstance());
        configurer.configureEditorAreaDropListener(new EditorAreaDropAdapter(configurer.getWindow()));
    }

	@Override
	public void postWindowCreate() {
		super.postWindowCreate();
		getWindowConfigurer().getWindow().getShell().setMinimumSize(800, 430);
	}
	
	@Override
	public boolean preWindowShellClose() {
		return passStoreControl();
	}
}
