package com.luxoft.university.gui.app_config;

import org.eclipse.swt.dnd.*;
import org.eclipse.ui.*;
import org.eclipse.ui.part.*;

import com.luxoft.universitylogger.api.*;

import static com.luxoft.university.gui.utils.LogConstants.*;

/**
 * This adapter class extends org.eclipse.swt.dnd.DropTargetAdapter and provides 
 * implementations for the methods described by the DropTargetListener interface. 
 */
public class EditorAreaDropAdapter extends DropTargetAdapter {
	private static ILogger LOGGER = LoggerFactory.getLogger(EditorAreaDropAdapter.class);
	private final IWorkbenchWindow window;

	/**
	 * Public constructor accepting org.eclipse.ui.IWorkbenchWindow
	 * 
	 * @param window - current window
	 */
	public EditorAreaDropAdapter(IWorkbenchWindow window) {
		this.window = window;
	}

	
	@Override
	public void dragEnter(DropTargetEvent event) {
		event.detail = DND.DROP_COPY;
	}

	@Override
	public void dragOperationChanged(DropTargetEvent event) {
		event.detail = DND.DROP_COPY;
	}

	@Override
	public void drop(DropTargetEvent event) {
		final IWorkbenchPage page = window.getActivePage();
		
		doDrop(event, page);
	}
	
	private void doDrop(DropTargetEvent event, IWorkbenchPage page) {

		if (EditorInputTransfer.getInstance().isSupportedType(event.currentDataType)) {
			EditorInputTransfer.EditorInputData[] editorInputs =(EditorInputTransfer.EditorInputData[]) event.data;
			
			if (editorInputs == null) {
				return;
			}
			
			for (EditorInputTransfer.EditorInputData editorInputData : editorInputs) {
				
				IEditorInput editorInput = editorInputData.input;
				String editorId = editorInputData.editorId;
				openEditor(page, editorInput, editorId);
			}
		}
	}

	private IEditorPart openEditor(IWorkbenchPage page, IEditorInput editorInput, String editorId) {
		IEditorPart result = null;
		
		try {
			IEditorRegistry editorReg = PlatformUI.getWorkbench().getEditorRegistry();
			IEditorDescriptor editorDesc = editorReg.findEditor(editorId);
			
			if (editorDesc != null && !editorDesc.isOpenExternal()) {
				result = page.openEditor(editorInput, editorId);
				LOGGER.info(USER_DROPED_NODE_TO_EDITOR);
			}
			
		} catch (PartInitException e) {
			LOGGER.error(FAILD_DROP_NODE_TO_EDITOR, e);
		}

		return result;
	}
}
