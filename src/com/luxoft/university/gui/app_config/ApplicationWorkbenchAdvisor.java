package com.luxoft.university.gui.app_config;

import org.eclipse.ui.application.*;


/**
 * This class extends org.eclipse.ui.application.WorkbenchAdvisor.  
 * It is public base class for configuring the workbench 
 */
public class ApplicationWorkbenchAdvisor extends WorkbenchAdvisor {
	private static final String PERSPECTIVE_ID = "com.luxoft.university.perspective"; //$NON-NLS-1$

	@Override
    public WorkbenchWindowAdvisor createWorkbenchWindowAdvisor(IWorkbenchWindowConfigurer configurer) {
        return new ApplicationWorkbenchWindowAdvisor(configurer);
    }
    
    @Override
	public String getInitialWindowPerspectiveId() {
		return PERSPECTIVE_ID;
	}
}
