package com.luxoft.university.gui.app_config;

import org.eclipse.jface.action.*;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.actions.ActionFactory;
import org.eclipse.ui.actions.ActionFactory.IWorkbenchAction;
import org.eclipse.ui.application.*;

import com.luxoft.university.gui.actions.*;
import com.luxoft.university.gui.actions.settings.ActionType;


/**
 * This class extends org.eclipse.ui.application.ActionBarAdvisor.  
 * It configures the action bars of a workbench window. 
 */
public class ApplicationActionBarAdvisor extends ActionBarAdvisor {
	private static final String FILE = "File";
	private static final String EDIT = "Edit";
	private static final String HELP = "Help";
	private IWorkbenchAction exitAction;
	private IWorkbenchAction aboutAction;
	private IWorkbenchAction openAction;
	private IWorkbenchAction newPersonAction;
	private IWorkbenchAction newGroupAction;
	private IWorkbenchAction saveEntityAction;
	private IWorkbenchAction newFileAction;
	private IWorkbenchAction openFileAction;
	private IWorkbenchAction saveFileAction;
	private IWorkbenchAction deleteEntityAction;
	private IWorkbenchAction saveAllEntities;

	/**
	 * Public constructor
	 * 
	 * @param configurer - Interface providing special access for configuring 
	 * the action bars of a workbench window.
	 */
	public ApplicationActionBarAdvisor(IActionBarConfigurer configurer) {
		super(configurer);
	}
	
	
	@Override
	protected void makeActions(IWorkbenchWindow window) {
		ContextAccesser windowWrapper = new ContextAccesser(window);
		register(openAction = new OpenAction(windowWrapper, ActionType.OPEN_NODE));
		register(newGroupAction = new NewGroupAction(windowWrapper, ActionType.NEW_GROUP));
		register(newPersonAction = new NewPersonAction(windowWrapper, ActionType.NEW_PERSON));
		register(exitAction = ActionFactory.QUIT.create(window));
		register(aboutAction = new AboutAction(ActionType.ABOUT));
		register(saveEntityAction = new SaveEntityAction(windowWrapper, ActionType.SAVE_NODE));
		register(saveFileAction = new SaveFileAction(windowWrapper, ActionType.SAVE_FILE));
		register(openFileAction = new OpenFileAction(windowWrapper, ActionType.OPEN_File));
		register(newFileAction = new NewFileAction(windowWrapper, ActionType.NEW_FILE));
		register(deleteEntityAction = new DeleteEntityAction(windowWrapper, ActionType.DELETE_NODE));
		register(saveAllEntities = new SaveAllEntitiesAction(windowWrapper, ActionType.SAVE_ALL_NODES));
	}
	
	@Override
	protected void fillMenuBar(IMenuManager menuBar) {
		MenuManager fileMenu = new MenuManager(FILE, FILE.toLowerCase());
				fileMenu.add(newFileAction);
				fileMenu.add(openFileAction);
				fileMenu.add(saveFileAction);
		MenuManager univerMenu = new MenuManager(EDIT, EDIT.toLowerCase());
				univerMenu.add(openAction);
				univerMenu.add(saveEntityAction);
				univerMenu.add(saveAllEntities);
				univerMenu.add(new Separator());
				univerMenu.add(newGroupAction);
				univerMenu.add(newPersonAction);
				univerMenu.add(new Separator());
				univerMenu.add(deleteEntityAction);
		MenuManager helpMenu = new MenuManager(HELP, HELP.toLowerCase());
				helpMenu.add(aboutAction);
				helpMenu.add(exitAction);
				
				menuBar.add(fileMenu);
				menuBar.add(univerMenu);
				menuBar.add(helpMenu);
	}
	
	@Override
	protected void fillCoolBar(ICoolBarManager coolBar) {
		coolBar.setLockLayout(false);
		
		IToolBarManager viewerToolbar = new ToolBarManager(coolBar.getStyle());
		coolBar.add(viewerToolbar);
		
		viewerToolbar.add(openAction);
		viewerToolbar.add(newGroupAction);
		viewerToolbar.add(newPersonAction);
		viewerToolbar.add(deleteEntityAction);
		viewerToolbar.add(new Separator());
		viewerToolbar.add(saveEntityAction);
		viewerToolbar.add(saveAllEntities);
	}
}