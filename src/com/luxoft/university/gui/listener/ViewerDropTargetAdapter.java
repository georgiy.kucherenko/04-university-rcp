package com.luxoft.university.gui.listener;

import static com.luxoft.university.api.utils.NodeUtilities.*;
import static com.luxoft.university.gui.utils.LogConstants.*;

import java.util.*;

import org.eclipse.jface.viewers.*;
import org.eclipse.swt.dnd.*;
import org.eclipse.ui.*;

import com.luxoft.university.api.models.*;
import com.luxoft.university.gui.editors.*;
import com.luxoft.universitylogger.api.*;


/**
 * Class extends org.eclipse.swt.dnd.DropTargetAdapter. 
 * Instances of this class are used in University App.
 */
public class ViewerDropTargetAdapter extends DropTargetAdapter {
	private final static ILogger LOGGER = LoggerFactory.getLogger(CourseEditor.class);
	private final TreeViewer treeViewer;

	/**
	 * Public constructor
	 * 
	 * @param treeViewer - viewer with tree-model
	 */
	public ViewerDropTargetAdapter(TreeViewer treeViewer) {
		this.treeViewer = treeViewer;
	}
	
	@Override
	public void dragOver(DropTargetEvent event) {
		event.feedback = DND.FEEDBACK_EXPAND | DND.FEEDBACK_SCROLL;
		BaseNode currentNode = (BaseNode) ((TreeSelection) treeViewer.getSelection()).getFirstElement();
		
		if (event.item == null || !(event.item.getData() instanceof GroupNode) 
				|| verifyTargetIsChild(currentNode, event)) { 
			event.detail = DND.DROP_NONE;
		} else {
			event.detail = DND.DROP_DEFAULT;
		}
		
		if (event.item != null) {
			event.feedback |= DND.FEEDBACK_SELECT;
		}
	}

	@Override
	public void drop(DropTargetEvent event) {
		if (TextTransfer.getInstance().isSupportedType(event.currentDataType)) {
		
			if (event.data == null) {
				event.detail = DND.DROP_NONE;
				return;
			}
			
			BaseNode targetNode = (BaseNode) event.item.getData();
			String transferedNodeId = (String)event.data;
			BaseNode root = targetNode.getParent() == null ? targetNode : targetNode.getRoot();
			BaseNode currentNode = getNodeById(root, transferedNodeId);
			currentNode.updateParent((GroupNode) targetNode);
			Optional<IEditorPart> editor = findEditorByNode(currentNode);
			
			if (editor.isPresent()) {
				((CourseEditor) editor.get()).updateParentField();
			}
			
			treeViewer.refresh();
			LOGGER.info(SUCCESSFUL_DROP_NODE);
		}
	}

	private Optional<IEditorPart> findEditorByNode(BaseNode currentNode) {
		IEditorPart[] dirtyEditors = PlatformUI.getWorkbench().getActiveWorkbenchWindow()
										.getActivePage().getDirtyEditors();
		
		return Arrays.asList(dirtyEditors).stream()
				.filter(editor -> (currentNode.getId().equals(getNodeFromEditor(editor).getId())))
				.findFirst();
	}
	
	private BaseNode getNodeFromEditor(IEditorPart editor) {
		return ((CourseEditorInput)editor.getEditorInput()).getNode();
	}

	private boolean verifyTargetIsChild(BaseNode currentNode, DropTargetEvent event) {
		return currentNode instanceof GroupNode 
				&& verifyRelationship((GroupNode) currentNode, (BaseNode)event.item.getData());
	}
}
	

