package com.luxoft.university.gui.listener;

/**
 * Enum with types of events that are used with CourseView listeners.
 */
public enum EventTypes {
	UPDATE_SAVE_ENABLE_STATUS, 
	UPDATE_SAVE_ALL_ENABLE_STATUS, 
	MODEL_PRESENCE_UPDATED
}
