package com.luxoft.university.gui.listener;

/**
 * Instances of this interface are listening to events fires on view
 * of University App.
 */
public interface CourseViewListener {
	
	/**
	 * Event handler for CourseView events
	 * 
	 * @param eventType - type of event
	 */
	void handleEvent(EventTypes eventType);

}
