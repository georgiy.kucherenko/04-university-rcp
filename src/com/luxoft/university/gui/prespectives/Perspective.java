package com.luxoft.university.gui.prespectives;

import org.eclipse.ui.*;

import com.luxoft.university.gui.views.CourseView;


/**
 * This class implements org.eclipse.ui.IPerspectiveFactory. 
 * It generates the initial page layout and visible actionset for a page
 */
public class Perspective implements IPerspectiveFactory {

	@Override	
	public void createInitialLayout(IPageLayout layout) {
		layout.setEditorAreaVisible(true);
		layout.addView(CourseView.ID, IPageLayout.LEFT, 0.3f, layout.getEditorArea());
	}
}