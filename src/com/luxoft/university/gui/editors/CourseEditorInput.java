package com.luxoft.university.gui.editors;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.ui.*;

import com.luxoft.university.api.models.BaseNode;


/**
 * This class implements org.eclipse.ui.IEditorInput. 
 * It is used in University App. All the methods are overriden. 
 * Except for public getter for input value.
 */
public class CourseEditorInput implements IEditorInput, IPersistableElement {
	public static final String MEMENTO_KEY_NODE_ID = "nodeId";
	private BaseNode node; 

	/**
	 * Public constructor 
	 * 
	 * @param node - the node to be presented in Editor.
	 */
	public CourseEditorInput(BaseNode node) {
		this.node = node;
	}
	

	@Override
	public boolean exists() {
		return false;
	}

	@Override
	public String getToolTipText() {
		return node.getName();
	}

	@Override
	public ImageDescriptor getImageDescriptor() {
		return null;
	}

	@Override
	public String getName() {
		return node.getName();
	}

	@Override
	public IPersistableElement getPersistable() {
		return this;
	}

	@Override
	public boolean equals(Object obj) {
		if (super.equals(obj))
			return true;
		if (!(obj instanceof CourseEditorInput))
			return false;
		CourseEditorInput other = (CourseEditorInput) obj;
		return node.getId().equals(other.node.getId());
	}

	@Override
	public int hashCode() {
		return node.hashCode();
	}

	@Override
	public <T> T getAdapter(Class<T> adapter) {
		return null;
	}
	
	@Override
	public void saveState(IMemento memento) {
		memento.putString(MEMENTO_KEY_NODE_ID, getNode().getId());
	}

	@Override
	public String getFactoryId() {
		return CourseEditorInputFactory.ID;
	}

	/**
	 * Public getter
	 * 
	 * @return the node that is encapsulated in this class
	 */
	public BaseNode getNode() {
		return node;
	}
}
