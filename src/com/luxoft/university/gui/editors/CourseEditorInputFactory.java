package com.luxoft.university.gui.editors;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.ui.IElementFactory;
import org.eclipse.ui.IMemento;
import org.eclipse.ui.PlatformUI;

import com.luxoft.university.api.models.BaseNode;
import com.luxoft.university.api.utils.NodeUtilities;
import com.luxoft.university.gui.views.CourseView;

public class CourseEditorInputFactory implements IElementFactory {
	public static final String ID = "com.luxoft.university.gui.editors.courseeditorinputfactory";
	
	@Override
	public IAdaptable createElement(IMemento memento) {
		String id = memento.getString(CourseEditorInput.MEMENTO_KEY_NODE_ID);
		
		BaseNode node = NodeUtilities.getNodeById(
				((CourseView)PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().findView(CourseView.ID))
					.getRoot(), 
				id
				);

		return id == null ? null : new CourseEditorInput(node);
	}
}
