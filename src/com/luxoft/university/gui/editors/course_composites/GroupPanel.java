package com.luxoft.university.gui.editors.course_composites;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.*;
import org.eclipse.swt.layout.*;
import org.eclipse.swt.widgets.*;

import com.luxoft.university.api.models.*;
import com.luxoft.university.gui.editors.*;
import com.luxoft.university.gui.utils.*;

/**
 * This class extends org.eclipse.swt.widgets.Composite. 
 * It represents set of widgets for showing GroupNode entity of University App.
 * This composite is used in CourseEditor object of University App.
 */
public class GroupPanel extends Composite{
	private Text txtName;
	private Text txtGroup;
	private Procedure parentUpdater;
	
	/**
	 * Public constructor
	 * 
	 * @param parent - parent composite from CourseEditor (EditorPart) object
	 * @param courseEditorInput - input of CourseEditor
	 */
	public GroupPanel(Composite parent, CourseEditor editor) {
		super(parent, SWT.None);
		setLayout(new FillLayout());
		createContent(parent, editor);
		addListeners(editor);
		initParentUpdater(editor);
	}
	
	/**
	 * Public method. Sets focus to Text widget for filling the name of Group. 
	 * This widget is usually first filled in by user
	 */
	public void setDefaultFocus() {
		txtName.setFocus();
	}
	
	/**
	 * Public getter for value of txtName
	 * 
	 * @return txtName value
	 */
	public String getTxtNameValue() {
		return txtName.getText();
	}

	/**
	 * Public getter for value of txtGroup
	 * 
	 * @return txtGroup value
	 */
	public String getTxtGroupValue() {
		return txtGroup.getText();
	}
	
	/**
	 * Method updates textField with parentGroup name
	 */
	public void udateTxtGroup() {
		parentUpdater.perform();
	}
	
	private void initParentUpdater(CourseEditor editor) {
		parentUpdater = () -> {
			BaseNode node = ((CourseEditorInput) editor.getEditorInput()).getNode();
			txtGroup.setText(node.getParent().getName());
		};
	}
	
	private void createContent(Composite parent, CourseEditor editor) {
		Composite composite = new Composite(this, SWT.None);
		composite.setLayout(new GridLayout(2, false));

		createNameLabel(composite);
		createNameText(composite);
		createGroupLabel(composite);
		createGroupText(composite);

		updateStateWithNodeData(editor);
	}

	private void updateStateWithNodeData(CourseEditor editor) {
		BaseNode node = ((CourseEditorInput)editor.getEditorInput()).getNode();
		
		txtName.setText(node.getName());
		txtGroup.setText(node.getParent().getName());
	}
	
	private void addListeners(CourseEditor editor) {
		ModifyListener listener = a -> editor.notifyModifiyng();
		
		txtName.addModifyListener(listener);
		txtGroup.addModifyListener(listener);
	}

	private void createGroupText(Composite composite) {
		txtGroup = new Text(composite, SWT.BORDER | SWT.WRAP | SWT.V_SCROLL );
		GridData data = new GridData(SWT.BEGINNING, SWT.CENTER, false, false);
		data.widthHint = 150;  
		data.heightHint = 40;
		txtGroup.setLayoutData(data);
	}

	private void createGroupLabel(Composite composite) {
		Label lblGroup = new Label(composite, SWT.None);
		lblGroup.setText("parent: ");
		lblGroup.setLayoutData(new GridData(GridData.VERTICAL_ALIGN_BEGINNING));
	}

	private void createNameText(Composite composite) {
		txtName = new Text(composite, SWT.BORDER | SWT.WRAP| SWT.V_SCROLL);
		GridData data = new GridData(SWT.BEGINNING, SWT.CENTER, false, false);
		data.widthHint = 150;  
		data.heightHint = 40;
		txtName.setLayoutData(data);
	}

	private void createNameLabel(Composite composite) {
		Label lblName = new Label(composite, SWT.None);
		lblName.setText("name: ");
		lblName.setLayoutData(new GridData(GridData.VERTICAL_ALIGN_BEGINNING));
	}
}
