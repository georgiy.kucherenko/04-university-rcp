package com.luxoft.university.gui.editors.course_composites;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.*;
import org.eclipse.swt.graphics.*;
import org.eclipse.swt.layout.*;
import org.eclipse.swt.widgets.*;

import com.luxoft.university.Application;
import com.luxoft.university.api.models.*;
import com.luxoft.university.gui.editors.*;
import com.luxoft.university.gui.utils.*;
import com.luxoft.university.gui.utils.FileDialogWrapper.WrapperType;

import static com.luxoft.university.gui.utils.ImageScaler.scaleImage;
import static org.eclipse.jface.resource.ImageDescriptor.createFromFile;
import static org.eclipse.ui.plugin.AbstractUIPlugin.imageDescriptorFromPlugin;


/**
 * This class extends org.eclipse.swt.widgets.Composite. 
 * It represents set of widgets for showing PersonNode entity of University App.
 * This composite is used in CourseEditor object of University App.
 */
public class PersonPanel extends Composite {
	private Text txtName;
	private Text txtGroup;
	private Text txtAddress;
	private Text txtCity;
	private Text txtResult;
	private String photoPath;
	private Label imageLabel;
	private Procedure parentUpdater;

	/**
	 * Public constructor
	 * 
	 * @param parent - parent composite from CourseEditor (EditorPart) object
	 * @param courseEditorInput - input of CourseEditor
	 */
	public PersonPanel(Composite parent, CourseEditor editor) {
		super(parent, SWT.None);
		setLayout(new FillLayout());
		
		Composite mainPanel = createMainPanel();
		
		setupDataPanel(mainPanel);
		updateStateWithPersonData(editor);
		setupImagePanel(editor, mainPanel);
		addListeners(editor);
		initParentUpdater(editor);
	}
	
	
	@Override
	public void dispose() {
		imageLabel.getImage().dispose();
		imageLabel.dispose();
		super.dispose();
	}
	
	/**
	 * Public method. Sets focus to Text widget for filling the name of Person. 
	 * This widget is usually first filled in by user
	 */
	public void setDefaultFocus() {
		txtName.setFocus();		
	}
	
	/**
	 * Public getter for value of PersonNode photo field. 
	 * Can contain either path to picture or be empty
	 * 
	 * @return photoPath value
	 */
	public String getPhotoPath() {
		return this.photoPath;
	}

	/**
	 * Public getter for value of txtGroup
	 * 
	 * @return txtGroup value
	 */
	public String getTxtGroupValue() {
		return txtGroup.getText();
	}

	/**
	 * Public getter for value of txtName
	 * 
	 * @return txtName value
	 */
	public String getTxtNameValue() {
		return txtName.getText();
	}

	/**
	 * Public getter for value of txtAddress
	 * 
	 * @return txtAddress value
	 */
	public String getTxtAddressValue() {
		return txtAddress.getText();
	}

	/**
	 * Public getter for value of txtCity
	 * 
	 * @return txtCity value
	 */
	public String getTxtCityValue() {
		return txtCity.getText();
	}

	/**
	 * Public getter for value of txtResult
	 * 
	 * @return txtResult value
	 */
	public String getTxtResultValue() {
		return txtResult.getText();
	}
	
	/**
	 * Method updates textField with parentGroup name
	 */
	public void udateTxtGroup() {
		parentUpdater.perform();
	}
	
	private void initParentUpdater(CourseEditor editor) {
		parentUpdater = () -> {
			BaseNode node = ((CourseEditorInput) editor.getEditorInput()).getNode();
			txtGroup.setText(node.getParent().getName());
		};
	}
	
	private void updatePersonImage() {
		imageLabel.setImage(scaleImage(getDisplay(), createFromFile(null, photoPath), IMAGE_MAX_WIDTH, IMAGE_MAX_HEIGHT));
	}
	
	private Composite createMainPanel() {
		Composite panel = new Composite(this, SWT.NONE);
		GridLayout layout = new GridLayout(2, true);
		panel.setLayout(layout);
		return panel;
	}

	private void setupDataPanel(Composite panel) {
		Composite wrappingPanel = createWrappingPanel(panel);
		
		Composite dataComposite = new Composite(wrappingPanel, SWT.CENTER);
		dataComposite.setLayout(new GridLayout(2, false));

		createNameLabel(dataComposite);
		createNameText(dataComposite);
		createGroupLabel(dataComposite);
		createGroupText(dataComposite);
		createAddressLabel(dataComposite);
		createAddressText(dataComposite);
		createCityLabel(dataComposite);
		createCityText(dataComposite);
		createResultLabel(dataComposite);
		createResultText(dataComposite);
	}
	
	private Composite createWrappingPanel(Composite panel) {
		Composite wrappingPanel = new Composite(panel, SWT.FILL);
		wrappingPanel.setLayout(new FillLayout());
		
		GridData data = new GridData();
		data.horizontalAlignment = GridData.CENTER;
		wrappingPanel.setLayoutData(data);
		return wrappingPanel;
	}


	private void updateStateWithPersonData(CourseEditor editor) {
		PersonNode person = (PersonNode) (((CourseEditorInput) editor.getEditorInput()).getNode());
		
		txtName.setText(person.getName());
		txtGroup.setText(person.getParent().getName());
		txtAddress.setText(person.getAddress());
		txtCity.setText(person.getCity());
		txtResult.setText(person.getResult() == -1 ? "" : person.getResult().toString());
		photoPath = person.getPhoto();
	}
	
	private void setupImagePanel(CourseEditor editor, Composite panel) {
		Composite imagePanel = new Composite(panel, SWT.FILL);
		tuneImagePanel(imagePanel);
		createImageLabel(imagePanel, editor);
	}
	
	private void addListeners(CourseEditor editor) {
		ModifyListener modifyListener = a -> editor.notifyModifiyng();
		
		txtName.addModifyListener(modifyListener);
		txtGroup.addModifyListener(modifyListener);
		txtAddress.addModifyListener(modifyListener);
		txtCity.addModifyListener(modifyListener);
		txtResult.addModifyListener(modifyListener);
		imageLabel.addMouseListener(createMouseListener(editor));
		imageLabel.addDisposeListener(createDisposeListener());
	}

	private void createResultText(Composite dataComposite) {
		txtResult = new Text(dataComposite, SWT.BORDER | SWT.MULTI | SWT.WRAP);
		GridData data = new GridData(SWT.BEGINNING, SWT.CENTER, false, false);
		data.widthHint = 170;  
		txtResult.setLayoutData(data);
	}

	private void createResultLabel(Composite dataComposite) {
		Label lblresult = new Label(dataComposite, SWT.None);
		lblresult.setText(RESULT_MESSAGE);
	}

	private void createCityText(Composite dataComposite) {
		txtCity = new Text(dataComposite, SWT.MULTI | SWT.WRAP | SWT.TRANSPARENT | SWT.BORDER | SWT.V_SCROLL);
		GridData data2 = new GridData(SWT.BEGINNING, SWT.CENTER, false, false);
		data2.widthHint = 150;  
		data2.heightHint = 40;
		txtCity.setLayoutData(data2);
	}

	private void createCityLabel(Composite dataComposite) {
		Label lblCity = new Label(dataComposite, SWT.None);
		lblCity.setText(CITY_MESSAGE);
		lblCity.setLayoutData(new GridData(GridData.VERTICAL_ALIGN_BEGINNING));
	}

	private void createAddressText(Composite dataComposite) {
		txtAddress = new Text(dataComposite, SWT.BORDER | SWT.MULTI | SWT.WRAP| SWT.V_SCROLL);
		GridData data = new GridData(SWT.BEGINNING, SWT.CENTER, false, false);
		data.widthHint = 150;  
		data.heightHint = 60;
		txtAddress.setLayoutData(data);
	}

	private void createAddressLabel(Composite dataComposite) {
		Label lblAddress = new Label(dataComposite, SWT.None);
		lblAddress.setText(ADDRESS_MESSAGE);
		lblAddress.setLayoutData(new GridData(GridData.VERTICAL_ALIGN_BEGINNING));
	}

	private void createGroupText(Composite dataComposite) {
		txtGroup = new Text(dataComposite, SWT.BORDER | SWT.WRAP | SWT.V_SCROLL );
		GridData data = new GridData(SWT.BEGINNING, SWT.CENTER, false, false);
		data.widthHint = 150;  
		data.heightHint = 40;
		txtGroup.setLayoutData(data);
	}

	private void createGroupLabel(Composite dataComposite) {
		Label lblGroup = new Label(dataComposite, SWT.None);
		lblGroup.setText(FOLDER_MESSAGE);
		lblGroup.setLayoutData(new GridData(GridData.VERTICAL_ALIGN_BEGINNING));
	}

	private void createNameText(Composite dataComposite) {
		txtName = new Text(dataComposite, SWT.BORDER | SWT.WRAP| SWT.V_SCROLL);
		GridData data = new GridData(SWT.BEGINNING, SWT.CENTER, false, false);
		data.widthHint = 150;  
		data.heightHint = 40;
		txtName.setLayoutData(data);
	}

	private void createNameLabel(Composite dataComposite) {
		Label lblName = new Label(dataComposite, SWT.None);
		lblName.setText(NAME_MESSAGE);
		lblName.setLayoutData(new GridData(GridData.VERTICAL_ALIGN_BEGINNING));
	}

	private void createImageLabel(Composite imagePanel, CourseEditor editor) {
		imageLabel = new Label(imagePanel, SWT.RIGHT);
		imageLabel.setImage(createPersonImage(editor));
		imageLabel.setCursor(new Cursor(getDisplay(), SWT.CURSOR_HAND));
	}

	private Image createPersonImage(CourseEditor editor) {
		ImageDescriptor descriptor = provideImageDescriptor(editor);
		return scaleImage(getDisplay(), descriptor, IMAGE_MAX_WIDTH, IMAGE_MAX_HEIGHT);

	}

	private ImageDescriptor provideImageDescriptor(CourseEditor editor) {
		CourseEditorInput input = (CourseEditorInput) editor.getEditorInput();
		PersonNode person = (PersonNode)input.getNode();
		
		if(person.getPhoto().isBlank()) {
			return imageDescriptorFromPlugin(Application.PLUGIN_ID, ImageConstants.DEFAULT_AVA_LOCATION.getFilePath());
		} else {
			return createFromFile(null, person.getPhoto());
		}
	}

	private void tuneImagePanel(Composite imagePanel) {
		imagePanel.setLayout(new FillLayout());
		
		GridData gridData = new GridData(GridData.FILL_HORIZONTAL);
		gridData.verticalAlignment = SWT.BEGINNING;
		gridData.horizontalAlignment = SWT.CENTER;
		imagePanel.setLayoutData(gridData);
	}
	
	private DisposeListener createDisposeListener() {
		return e -> {
			if (imageLabel.getImage() != null && !imageLabel.getImage().isDisposed()) {
				imageLabel.getImage().dispose();
			}
			imageLabel.getCursor().dispose();
		};
	}

	private MouseListener createMouseListener(CourseEditor editor) {
		return new MouseAdapter() {

			@Override
			public void mouseDown(MouseEvent e) {
				super.mouseDown(e);
				FileDialogWrapper dlg = new FileDialogWrapper(WrapperType.IMAGE_FILES, SWT.OPEN);
				
				if (dlg.open() != null) {
					imageLabel.getImage().dispose();
					photoPath = dlg.getFullPath();
					updatePersonImage();
					editor.notifyModifiyng();
				}
			}
		};
	}

	private static final String RESULT_MESSAGE = "Result: ";
	private static final String CITY_MESSAGE = "City: ";
	private static final String ADDRESS_MESSAGE = "Address: ";
	private static final String FOLDER_MESSAGE = "Folder: ";
	private static final String NAME_MESSAGE = "Name: ";
	private static final int IMAGE_MAX_WIDTH = 100;
	private static final int IMAGE_MAX_HEIGHT = 400;
}
