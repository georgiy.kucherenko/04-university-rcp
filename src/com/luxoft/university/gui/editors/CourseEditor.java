package com.luxoft.university.gui.editors;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.*;
import org.eclipse.ui.part.EditorPart;
import org.eclipse.ui.plugin.AbstractUIPlugin;

import com.luxoft.university.Application;
import com.luxoft.university.api.models.*;
import com.luxoft.university.gui.editors.course_composites.*;
import com.luxoft.university.gui.listener.*;
import com.luxoft.university.gui.utils.*;
import com.luxoft.university.gui.views.*;
import com.luxoft.universitylogger.api.*;

import static com.luxoft.university.gui.views.CourseView.*;
import static com.luxoft.university.gui.utils.LogConstants.*;


/**
 * This class extends org.eclipse.ui.part.EditorPart. 
 * It is used in University App as an editor for app's models
 */
public class CourseEditor extends EditorPart implements  ISaveablePart2, IPartListener2 {
	public static final String ID = "com.luxoft.university.editors.courseeditor";
	private final static ILogger LOGGER = LoggerFactory.getLogger(CourseEditor.class);
	private final static String QUESTION_TITLE = "Save changes";
	private static final String BLANK_QUESTION = "\n\n Save changes?";
	private static final String ERROR_SAVING_NODE = "ERROR on saving entity";
	private Composite mainPanel;

	
	@Override
	public void doSave(IProgressMonitor monitor) {
		try {
			BaseNode currentNode = getNode();
			
			if (currentNode instanceof GroupNode) {
				GroupPanel panel = (GroupPanel) mainPanel; 
				GroupNode group = (GroupNode) currentNode;
				
				group.updateState(
						panel.getTxtNameValue(),
						panel.getTxtGroupValue()
						);

			} else if (currentNode instanceof PersonNode) {
				PersonPanel panel = ((PersonPanel) mainPanel);
				PersonNode person = (PersonNode) currentNode;
				
				person.udateState(
						panel.getTxtNameValue(),
						panel.getTxtGroupValue(),
						panel.getTxtAddressValue(),
						panel.getTxtCityValue(),
						panel.getTxtResultValue().isEmpty() ? -1 : Integer.valueOf(panel.getTxtResultValue()),
						panel.getPhotoPath()
						);
			}
			
			setPartName(currentNode.getName());
			
			getView().refreshViewer();
			getView().setPartProperty(PROP_EDITED, PROP_EDITED_TRUE);
			
		} catch (Exception e) {
			new ExceptionNotifier(e);
			LOGGER.error(FAIL_SAVING_NODE, e);
		}
		firePropertyChange(IEditorPart.PROP_DIRTY);
	}

	@Override
	public void doSaveAs() {
	}

	@Override
	public void init(IEditorSite site, IEditorInput input) throws PartInitException {
		setSite(site);
		setInput(input);
		setPartName(input.getName());
	}

	@Override
	public boolean isDirty() {
			BaseNode currentNode = getNode();
			
			if (currentNode instanceof GroupNode) {
				GroupPanel panel = (GroupPanel) mainPanel; 

				if (!currentNode.getName().equals(panel.getTxtNameValue())
					|| !(currentNode.getParent().getName()).equals(panel.getTxtGroupValue())){
					return true;
				}
				

			} else if (currentNode instanceof PersonNode) {
				PersonNode person = (PersonNode) currentNode;
				PersonPanel panel = ((PersonPanel) mainPanel);
				
				if (!person.getName().equals(panel.getTxtNameValue())
						|| !(person.getParent().getName()).equals(panel.getTxtGroupValue())
						|| !(person.getAddress().equals(panel.getTxtAddressValue()))
						|| !(person.getCity().equals(panel.getTxtCityValue()))
						|| !(person.getPhoto().equals(panel.getPhotoPath()))
						|| !(person.getResult().equals(getResultFromPanel(panel)))){
					return true;
				}
			}

			return false;
	}

	@Override
	public boolean isSaveAsAllowed() {
		return false;
	}
	
	@Override
	public void dispose() {
		if (getView() != null) {
			getView().fireEvent(EventTypes.UPDATE_SAVE_ENABLE_STATUS);
		}
		getTitleImage().dispose();
		super.dispose();
	}

	@Override
	public void createPartControl(Composite parent) {
		parent.setLayout(new FillLayout());
		BaseNode baseNode = getNode();
		String imageFilePath = null;
		
		if (baseNode instanceof PersonNode) {
			mainPanel = new PersonPanel(parent, this);
			imageFilePath = ImageConstants.PERSON_NODE.getFilePath();
		} else if (baseNode instanceof GroupNode) {
			mainPanel = new GroupPanel(parent, this);
			imageFilePath = ImageConstants.GROUP_NODE.getFilePath();
		}
		setTitleImage(AbstractUIPlugin.imageDescriptorFromPlugin(Application.PLUGIN_ID, imageFilePath).createImage());
	}

	@Override
	public void setFocus() {
		if (getNode() instanceof GroupNode) {
			((GroupPanel) mainPanel).setDefaultFocus();
		} else {
			((PersonPanel) mainPanel).setDefaultFocus();
		}
		notifyModifiyng();
	}
	
	@Override
	public int promptToSaveOnClose() {
		showThisEditor();
		
		final int dialogResult = new YesNoCancelDialog(QUESTION_TITLE, createQuestionMessage()).open();
		
		if (dialogResult == YesNoCancelDialog.YES) {
			
			if (performValidation()) {
				return ISaveablePart2.YES;
			}
			
			new ExceptionNotifier(ERROR_SAVING_NODE);
			
		} else if (dialogResult == YesNoCancelDialog.NO) {
			return ISaveablePart2.NO;
		} 
		
		return ISaveablePart2.CANCEL;
	}
	

	/**
	 * Method validates the editor
	 * 
	 * @return true is validation successful, otherwise - false
	 */
	public boolean performValidation() {
		BaseNode currentNode = getNode();
			
		if (currentNode instanceof GroupNode) {
			GroupPanel panel = (GroupPanel) mainPanel; 
		
			if (panel.getTxtNameValue().isBlank() || panel.getTxtGroupValue().isBlank()) {
				return false;
			}

		} else if (currentNode instanceof PersonNode) {
			PersonPanel panel = (PersonPanel) mainPanel;
				
			if (panel.getTxtNameValue().isBlank() || panel.getTxtGroupValue().isBlank()) {
				return false;
			}
		}
		return true;
	}
	
	/**
	 * Method notifies View about changings in editors 
	 */
	public void notifyModifiyng() {
		firePropertyChange(IEditorPart.PROP_DIRTY);
		getView().fireEvent(EventTypes.UPDATE_SAVE_ENABLE_STATUS);
	}
	
	/**
	 * Method updates parentGroup name in panel. 
	 * Used during DND
	 */
	public void updateParentField() {
		BaseNode currentNode = getNode();
		
		if (currentNode instanceof GroupNode) {
			((GroupPanel) mainPanel).udateTxtGroup(); 

		} else if (currentNode instanceof PersonNode) {
			((PersonPanel) mainPanel).udateTxtGroup();
		}
	}
	
	private BaseNode getNode() {
		return getInput().getNode();
	}
	
	private CourseEditorInput getInput() {
		return (CourseEditorInput) getEditorInput();
	}
	
	private String createQuestionMessage() {
		BaseNode currentNode = getNode() instanceof PersonNode ? 
				(PersonNode)getNode() : (GroupNode)getNode();
		
		return currentNode.toString() + BLANK_QUESTION;
	}
	
	private void showThisEditor() {
		try { 
			getSite().getPage().openEditor(getInput(), CourseEditor.ID);
		} catch (PartInitException e) {
			new ExceptionNotifier(e);
			LOGGER.error(ERROR_OPENING_EDITOR, e);
		}
	}
	
	private CourseView getView() {
		return (CourseView) getSite().getPage().findView(CourseView.ID);
	}
	
	private Integer getResultFromPanel(PersonPanel panel) {
		return panel.getTxtResultValue().isEmpty() ?
				-1 : Integer.parseInt(panel.getTxtResultValue());
	}
}