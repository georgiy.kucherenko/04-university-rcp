package com.luxoft.university.gui.utils;

import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.ui.PlatformUI;

/**
 * Wrapper for org.eclipse.swt.widgets.FileDialog. 
 * Also contains nested enum class with dialog types.
 */
public class FileDialogWrapper {
	private final FileDialog dialog;

	/**
	 * Public constructor
	 * 
	 * @param type - type of Dialog
	 * @param style - style constant from SWT class
	 */
	public FileDialogWrapper(WrapperType type, int style) {
		dialog = new FileDialog(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell(), style);
		dialog.setFilterNames(type.getNames());
		dialog.setFilterExtensions(type.getExtensions());
	}
	
	/**
	 * Method-delegat for open() method of FileDialog 
	 * @return a string describing the absolute path of 
	 * the first selected file,or null if the dialog was cancelled or an error occurred
	 */
	public String open() {
		return dialog.open();
	}
	
	/**
	 * Method returning full path from dialog
	 * 
	 * @return full path to file
	 */
	public String getFullPath() {
		return String.format("%s\\%s", dialog.getFilterPath(), dialog.getFileName());
	}
	
	/**
	 * Enum containing types of FileDialogWrapper
	 */
	public static enum WrapperType {
		COURSE_FILES (new String[] { "Course Files (*.crs)" }, new String[] { "*.crs" }),
		IMAGE_FILES (new String[] { "Images (*.png)", "Images (*.jpg)", "Images (*.bmp)"}, new String[] { "*.png", "*.jpg", "*.bmp" });

		private String[] names;
		private String[] extensions;
		
		WrapperType(String[] strings, String[] strings2) {
			this.names = strings;
			this.extensions = strings2;
		}
		
		/**
		 * Public getter 
		 * 
		 * @return array with descriptions of possible extensions of files 
		 */
		public String[] getNames() {
			return this.names;
		}
		
		/**
		 * Public getter 
		 * 
		 * @return array of file extensions
		 */
		public String[] getExtensions () {
			return this.extensions;
		}
	}
}
