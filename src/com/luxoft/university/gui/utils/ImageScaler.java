package com.luxoft.university.gui.utils;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.graphics.*;
import org.eclipse.swt.widgets.Display;


/**
 * Utility class for resizing the image to preferred size
 */
public final class ImageScaler {
	
	private ImageScaler() {
	}

	/**
	 * This method scales an image to preferred size
	 * 
	 * @param display - current display
	 * @param imageDesc - a source object for resizing
	 * @param maxWidth - maximum acceptable width of image
	 * @param maxHeight - maximum acceptable height of image
	 * @return
	 */
	public static Image scaleImage(Display display, ImageDescriptor imageDesc, int maxWidth, int maxHeight) {
		if (imageDesc == null)
			return null;

		ImageData imageData = imageDesc.getImageData(100);

		if (imageData == null) {
			return imageDesc.createImage();
		}

		int newHeight = maxHeight;
		int newWidth = (imageData.width * newHeight) / imageData.height;
		
		if (newWidth > maxWidth) {
			newWidth = maxWidth;
			newHeight = (imageData.height * newWidth) / imageData.width;
		}

		Image newImage = new Image(display, newWidth, newHeight);
		GC gc = new GC(newImage);
		Image oldImage = imageDesc.createImage();
		gc.drawImage(oldImage, 0, 0, imageData.width, imageData.height, 0, 0, newWidth, newHeight);
		ImageDescriptor result = ImageDescriptor.createFromImage(newImage);
		Image resultImage = result.createImage();
		oldImage.dispose();
		newImage.dispose();
		gc.dispose();
		return resultImage;
	}
}
