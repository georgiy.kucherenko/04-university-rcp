package com.luxoft.university.gui.utils;

/**
 * Public enum that holds constants with paths to images that are used
 * in Univestity app
 * 
 */
public enum ImageConstants {
	PERSON_NODE ("icons/human.png"),
	GROUP_NODE ("icons/folder_image.png"), 
	DEFAULT_AVA_LOCATION ("icons/ava_1.png");
	
	private String filePath;

	private ImageConstants(String name) {
		this.filePath = name;
	}
	
	/**
	 * Public getter
	 * 
	 * @return path to image file
	 */
	public String getFilePath() {
		return filePath;
	}
}
