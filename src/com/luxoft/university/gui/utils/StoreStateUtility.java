package com.luxoft.university.gui.utils;

import static com.luxoft.university.gui.utils.YesNoCancelDialog.*;

import org.eclipse.ui.*;
import org.eclipse.ui.part.WorkbenchPart;

import com.luxoft.university.gui.actions.SaveFileAction;
import com.luxoft.university.gui.actions.settings.ActionUtility;
import com.luxoft.university.gui.views.CourseView;


/**
 * Utility class for handling saving state issues.  
 */
public final class StoreStateUtility {
	private static final String TITLE = "Save changes";
	private static final String QUESTION_MESSAGE = "Should we save changes to file?";

	private StoreStateUtility() {
	}
	
	/**
	 * This method check if the state is saved, proposes to save if needed, 
	 * also after-checks whether the save processes were finished successful. 
	 * If something goes wrong - returns false. 
	 * 
	 * @return whether storeControl passed successful. 
	 */
	public static boolean passStoreControl() {
		if (!verifyModelDirty() && !verifyDirtyEditorsExist()) {
			return true;
		}
		
		final int dialogResult = new YesNoCancelDialog(TITLE, QUESTION_MESSAGE).open();
		
		if (dialogResult == YES) { 
			ActionUtility.getActionById(SaveFileAction.ID).get().run();
			
			if (!verifyModelDirty()) {
				return true;
			}
			
		} else if (dialogResult == NO) { 
			getActivePage().closeAllEditors(false);
			return true;
		} 
		
		return false;
	}
	
	private static IWorkbenchPage getActivePage() {
		return PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
	}
	
	private static boolean verifyDirtyEditorsExist() {
		return getActivePage().getDirtyEditors().length > 0;
	}
	
	private static boolean verifyModelDirty() {
		WorkbenchPart view = (WorkbenchPart)getActivePage().findView(CourseView.ID);
		return view.getPartProperty(CourseView.PROP_EDITED).equals(CourseView.PROP_EDITED_TRUE);
	}
}
