package com.luxoft.university.gui.utils;

import org.eclipse.core.runtime.*;
import org.eclipse.jface.dialogs.ErrorDialog;
import org.eclipse.swt.widgets.Display;

import com.luxoft.university.Application;


/**
 * Instances of this class are used to inform user about some Exception situations. 
 * It just displays an Error dialog with provided message
 */
public class ExceptionNotifier {
	private static final String FAILED_PERFORMING_ACTION = "Faild pefroming action";
	private static final String DIALOG_TITLE = "University App Error";

	/**
	 * Public constructor
	 * 
	 * @param message - String value to be shown in ErrorDialog
	 */
	public ExceptionNotifier(String message) {
		Status status = new Status(IStatus.ERROR, Application.PLUGIN_ID, 0, message, null);
		ErrorDialog.openError(Display.getCurrent().getActiveShell(), DIALOG_TITLE, FAILED_PERFORMING_ACTION, status);
	}

	/**
	 * Public constructor
	 * 
	 * @param exception - exception instance that caused the situation
	 */
	public ExceptionNotifier(Exception exception) {
		this(exception.getMessage());
	}
}
