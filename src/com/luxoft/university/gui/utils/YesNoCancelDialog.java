package com.luxoft.university.gui.utils;

import static org.eclipse.jface.dialogs.IDialogConstants.*;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.ui.PlatformUI;

/**
 * This class extends org.eclipse.jface.dialogs.MessageDialog. 
 * It holds some constants and presets some settings to super constructor. 
 * All above helps to meet DRY concept  
 */
public class YesNoCancelDialog extends MessageDialog {
	public final static int YES = 0;
	public final static int NO = 1;
	public final static int CANCEL = 2;
	private final static String[] BUTTON_LABELS = new String[] {YES_LABEL, NO_LABEL, CANCEL_LABEL};
	
	/**
	 * Public constructor
	 * 
	 * @param title - title of dialog
	 * @param message - message for dialog
	 */
	public YesNoCancelDialog (String title, String message) {
		super(
				PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell(), 
				title, 
				null, 
				message, 
				MessageDialog.QUESTION, 
				BUTTON_LABELS, 
				YES
			);
	}
}
