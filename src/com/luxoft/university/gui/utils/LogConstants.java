package com.luxoft.university.gui.utils;

public class LogConstants {
	public static final String ERROR_OPENING_EDITOR = "error opening CourseEditor ";
	public static final String OPENED_EDITOR = "opened editor ";
	public static final String FAIL_VALIDATION_LOG = "failed validation during save node ";
	public static final String FAIL_SAVING_FILE = "failed saving file ";
	public static final String FAIL_OPENING_FILE = "failed opening file ";
	public static final String SUCCESSFUL_SAVING_FILE = "successfully saved file ";
	public static final String SUCCESSFUL_OPENING_FILE = "successfully opened file ";
	public static final String FAILD_DROP_NODE_TO_EDITOR = "failed drop node to editor ";
	public static final String USER_DROPED_NODE_TO_EDITOR = "user droped node to editor ";
	public static final String FAIL_SAVING_NODE = "failed saving node ";
	public static final String NEW_ROOT_NODE_IS_SET = "new root node is set ";
	public static final String SUCCESSFUL_DROP_NODE = "successfuly droped node to groupNode ";
	
	private LogConstants() {
	}
}
