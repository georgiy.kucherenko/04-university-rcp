package com.luxoft.university.api;

import com.luxoft.university.api.data_managers.DataManager;
import com.luxoft.university.api.exceptions.DataManagerException;
import com.luxoft.university.api.models.GroupNode;

/**
 * Service class operating with GroupNodes. 
 * Goals of this class are:
 * <p><ul>
 * <li>load GroupNode objects from some source</li>
 * <li>save GroupNode objects to some source</li>
 * <li>create blank Course object</li>
 * </ul>
 * @author Georgiy Kucherenko
 *
 */
public class UniverService {
	private final DataManager manager;

	/**
	 * public constructor 
	 * 
	 * @param manager - object of class that implements DataManager
	 */
	public UniverService(DataManager manager) {
		this.manager = manager;
	}

	/**
	 * Public method that loads GroupNode object from some source
	 * 
	 * @param path - source path (for example - full path to file)
	 * @return - GroupNode object
	 * @throws DataManagerException - in case of problems with deserialization, 
	 * or problems with source (for example with file) etc.
	 */
	public GroupNode loadData(String path) throws DataManagerException {
		return manager.loadData(path);
	}
	
	/**
	 * Public method. Saves GroupNode object to the specified path (file path for example)
	 * @param path - source path (for example - full path to file) 
	 * @param data - GroupNode object to be saved
	 * @return - true if saved successfully. In other case - returns false
	 * @throws DataManagerException - in case of troubles with source (for example with specified file path)
	 */
	public void saveData(String path, GroupNode data) throws DataManagerException {
		manager.saveData(path, data);
	}
	
	/**
	 * Creates and provides a new GroupNode object with name "ROOT" and with no children. 
	 * 
	 * @return - GroupNode object
	 */
	public GroupNode createRoot(){
		return manager.createRoot();
	}
}
