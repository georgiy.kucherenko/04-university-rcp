package com.luxoft.university.api.exceptions;

/**
 * This exception should be thrown by DataManager.
 * It is thrown when DataManager faces problems with loading or saving data from/to some source 
 * (this exceptions is not going to be serialized that's why SuppressWarnings annotation is applied) 
 * 
 * @author Georgiy Kucherenko
 *
 */
@SuppressWarnings("serial")
public class DataManagerException extends Exception {

	/**
	 * Public constructor
	 * 
	 * @param message - message of exception
	 * @param cause - the reason 
	 */
	public DataManagerException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * Public constructor
	 * 
	 * @param message - message of exception
	 */
	public DataManagerException(String message) {
		super(message);
	}
	
	
}

