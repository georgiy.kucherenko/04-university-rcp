package com.luxoft.university.api.exceptions;

/**
 * This exception should be thrown by FileDataManager.
 * FileDataManager can catch IOException or ClassNotFoundException during serialization or deserialization. 
 * And in this case FileDataManagerException is thrown with the message from caught exception and cause (reason). 
 * (this exceptions is not going to be serialized that's why SuppressWarnings annotation is applied) 
 * 
 * @author Georgiy Kucherenko
 *
 */
@SuppressWarnings("serial")
public class FileDataManagerException extends DataManagerException {

	/**
	 * Public constructor
	 * 
	 * @param message - message of exception
	 * @param cause - the reason 
	 */
	public FileDataManagerException(String message, Throwable cause) {
		super(message, cause);
	}
	
	/**
	 * Public constructor
	 * 
	 * @param message - message of exception
	 */
	public FileDataManagerException(String message) {
		super(message);
	}
	
}

