package com.luxoft.university.api.models;

import java.io.Serializable;
import java.util.*;

/**
 * A node of tree that is used in University App. 
 * This node can have both children nodes and leafs. 
 * Class also provides methods for GUI support 
 * (like getEntries() - for RCP View adapter, or updateState() - for easy updating of state)
 * 
 */
public class GroupNode extends BaseNode implements Serializable {
	private static final long serialVersionUID = 1L;
	private String id;
	private String name;
	private GroupNode parent;
	private final List<BaseNode> entries;

/**
 * Public constructor
 * 
 * @param name - name of node
 * @param parent - parent node
 */
	public GroupNode(String name, GroupNode parent) {
		this.id = UUID.randomUUID().toString();
		this.name = name;
		this.parent = parent;
		this.entries = new ArrayList<BaseNode>();
	}
	
	@Override
	public String getId() {
		return id;
	}
	
	@Override
	public String getName() {
		return name;
	}
	
	@Override
	public GroupNode getParent() {
		return parent;
	}

	@Override
	public String toString() {
		return "GroupNode [name=" + name 
				+ ", parent.name=" + (parent == null ? "null" : parent.getName()) 
				+ ", entries=[" + mapEntriesToString() + "]";
	}
	
	private String mapEntriesToString() {
		return entries.stream()
				.map(node -> node instanceof GroupNode ? 
						"GroupNode[name=" + node.getName() +"]":
							"PersonNode[name=" + node.getName()+"]")
				.reduce((result, node) -> result + ", " + node)
				.orElseGet(() -> "null");
	}
	
	@Override
	protected void setParent(GroupNode parent) {
		this.parent = parent;
	}

	/**
	 * Public setter 
	 * 
	 * @param name - sets the name of current node
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Method return array of children. This method is used in adapter for RCP View 
	 * 
	 * @return - array of children;
	 */
	public BaseNode[] getEntries() {
		if (entries != null) {
			return (BaseNode[]) entries.toArray(new BaseNode[entries.size()]);
		}
		return new BaseNode[] {};
	}
	
	/**
	 * Method adds a child to current node
	 * @param node - child to be added
	 */
	public void addChild(BaseNode node) {
		this.entries.add(node);
	}

	/**
	 * Method for updating state of current object
	 * 
	 * @param name - name to be set
	 * @param nameOfParent - name of parent to be set
	 */
	public void updateState(String name, String nameOfParent) {
		this.name = name;
		updateParent(nameOfParent);
	}
	
	List<BaseNode> getEntriesList() {
		return entries;
	}
	
	void removeChild(BaseNode baseNode) {
		entries.remove(
				entries.stream()
				.filter(a -> a.getId().equals(baseNode.getId()))
				.findFirst()
				.get()
				);
	}
}