package com.luxoft.university.api.models;

import java.io.Serializable;
import java.util.*;

/**
 * Abstraction for the tree model that is used in University App.
 * Some methods (like getDepth()) are provided in order to support GUI's functionality.
 */
public abstract class BaseNode implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * Public getter for id field
	 * @return id of Node
	 */
	public abstract String getId();
	
	/**
	 * Public getter for name field
	 * @return name of Node
	 */
	public abstract String getName();

	/**
	 * Public getter for parent field
	 * @return parent of Node
	 */
	public abstract GroupNode getParent();
	
	/**
	 * Public setter for parent field
	 * @param parent - parent node 
	 */

	@Override
	public String toString() {
		return "BaseNode [getName()=" + getName() + ", getParent().getName()=" + getParent().getName() + "]";
	}
	
	/**
	 * This method removes current object from children list of his parent
	 */
	public void detachFromParent() {
		getParent().removeChild(this);
	}
	
	/**
	 * This method returns the quantity of his parents (parent, grandparent etc.)
	 * @return - parents quantity (parent, grandparent etc.)
	 */
	public int getDepth() {
		int depth = 0;
		GroupNode parent = getParent();

		while (parent != null) {
			parent = parent.getParent();
			depth++;
		}
		return depth;
	}

	/**
	 * Method returns root node of the tree
	 * 
	 * @return root node of the tree
	 */
	public GroupNode getRoot() {
		GroupNode parentGroup=getParent();

		while (parentGroup.getParent() != null ) {
			parentGroup = parentGroup.getParent();
		}

		return parentGroup;
	}
	
	/**
	 * Method updates node's parent with given value
	 * 
	 * @param newParent - parent to be set
	 * @return false if current node is root node. Otherwise returns true
	 */
	public boolean updateParent(GroupNode newParent) {
		if (getParent() != null) {
			
			detachFromParent();
			newParent.addChild(this);
			setParent(newParent);
			return true;
		} 
		
		return false;
	}
	
	/**
	 * Method sets new Parent value to current node
	 * 
	 * @param parent - parent to be set
	 */
	protected abstract void setParent(GroupNode parent);
	
	void updateParent(String nameOfParent) {
		if (getParent().getName().equals(nameOfParent)) {
			verifyParentHasChild();
			return;
		}
		
		getParent().removeChild(this);
		bindParentByName(nameOfParent);
		getParent().addChild(this); 
	}
	
	private void verifyParentHasChild() {
		Optional<BaseNode> result = getParent().getEntriesList().stream()
		.filter(node -> node.getId().equals(this.getId()))
		.findFirst();
		
		if (result.isEmpty()) {
			getParent().addChild(this);
		}
	}

	private void bindParentByName(String groupName) {
		GroupNode root = getRoot();
		setParent(null);		

		if (!findParent(root.getEntriesList(), groupName)) {
			createParentAndBind(root, groupName);
		}
	}

	private void createParentAndBind(GroupNode root, String groupName) {
		GroupNode newGroup = new GroupNode(groupName, root);
		root.addChild(newGroup);
		setParent(newGroup);
	}

	private boolean findParent(List<BaseNode> entriesList, String groupName) {
		for (BaseNode node : entriesList) {
			
			if (getParent() != null) {
				return true;
			}
			
			if (node instanceof GroupNode) {
				if (node.getName().equals(groupName)) {
					setParent((GroupNode) node);
					return true;
				} else {
					findParent(((GroupNode) node).getEntriesList(), groupName);
				}
			}
			
		}
		return false;
	}
}