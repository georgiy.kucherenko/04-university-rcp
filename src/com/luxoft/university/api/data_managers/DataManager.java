package com.luxoft.university.api.data_managers;

import com.luxoft.university.api.exceptions.DataManagerException;
import com.luxoft.university.api.models.GroupNode;

/**
 * Abstraction for an object that can manage GroupNode object. 
 * Basically it can Load, Save and Create GroupNode objects. 
 * 
 * @author Georgiy Kucherenko
 *
 */
public interface DataManager {
	
	/**
	 * Abstract method for loading GroupNode from some Source.  
	 * 
	 * @param path - source path (for example: path to file)
	 * @return - GroupNode entity
	 * @throws DataManagerException in case of problems with path or with source. 
	 * Message with detailed information should be provided together with Exception
	 */
	GroupNode loadData(String path) throws DataManagerException;
	
	/**
	 * Abstract method for saving GroupNode to some destination
	 * 
	 * @param path - source path (for example: path to file)
	 * @param group - GroupNode entity
	 * @return - true if saved successfully. In other case returns false
	 * @throws DataManagerException in case of problems with path or with source. 
	 */
	void saveData(String path, GroupNode group) throws DataManagerException;

	/**
	 * Creates and provides a new GroupNode object with name "ROOT" and with no children.
	 * 
	 * @return - new GroupNode object with initialized fields
	 */
	default GroupNode createRoot() {
		return new GroupNode("ROOT", null);
	}
}
