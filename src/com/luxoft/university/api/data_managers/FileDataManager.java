package com.luxoft.university.api.data_managers;

import java.io.*;

import com.luxoft.university.api.exceptions.FileDataManagerException;
import com.luxoft.university.api.models.GroupNode;

/**
 * Implementation of DataManager interface. 
 * Dedicated to operating with Files as sources
 * 
 * @author Georgiy Kucherenko
 *
 */
public class FileDataManager implements DataManager {
	private static final String TRY_TO_SAVE_NULL = "Null cannot be saved to file";

	@Override
	public GroupNode loadData(String path) throws FileDataManagerException {
		try (ObjectInputStream oInputStream = new ObjectInputStream(new FileInputStream(path))) {
			return (GroupNode) oInputStream.readObject();
		} catch (IOException | ClassNotFoundException ex) {
			throw new FileDataManagerException(ex.getMessage(), ex);
		} 
	}

	@Override
	public void saveData(String path, GroupNode studyGroup)  throws FileDataManagerException {
		if (studyGroup == null) {
			throw new FileDataManagerException(TRY_TO_SAVE_NULL);
		}

		try (ObjectOutputStream oOutputStream = new ObjectOutputStream(new FileOutputStream(path))){
			oOutputStream.writeObject(studyGroup);
		} catch (IOException ex) {
			throw new FileDataManagerException(ex.getMessage(), ex);
		}
	}
}
