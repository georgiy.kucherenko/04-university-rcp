package com.luxoft.university.api.utils;

import java.util.*;

import com.luxoft.university.api.models.*;

/**
 * Utility class for working with entity-models of University App.
 * This class is also used for creating unit-tests for University App.
 */
public final class NodeUtilities {
	
	private NodeUtilities() {
	}

	/**
	 * Method verifies weather one entity is a parent of another entity
	 *  
	 * @param parentNode - supposed to be parent
	 * @param childNode - supposed to be child
	 * @return instance of Optional<BaseNode> 
	 */
	public static Optional<BaseNode> verifyParent(GroupNode parentNode, BaseNode childNode) {
		return Arrays.asList(parentNode.getEntries()).stream()
				.filter(a -> a.getId().equals(childNode.getId()))
				.findFirst();		
	}
	
	/**
	 * Method creating an instance of GroupNode. Also updates parent entity if created node is not root
	 * 
	 * @param name - name of created node
	 * @param parent - parent GroupNode for created GroupNode. If creating root node - should pass null.
	 * @return - created GroupNode
	 */
	public static GroupNode createGroupNode(String name, GroupNode parent) {
		GroupNode groupNode = new GroupNode(name, parent);
		
		if (parent != null && !name.isEmpty()) {
			parent.addChild(groupNode);
		}
		return groupNode;
	}
	
	/**
	 * Method creating a blank GroupNode (name of node is empty). 
	 * 
	 * @param parent - parent GroupNode for created GroupNode. If creating root node - should pass null.
	 * @return - created GroupNode
	 */
	public static GroupNode createBlankGroup(GroupNode parent) {
		return createGroupNode("", parent);
	}
	
	/**
	 * Method for creating PersonNode. Also updates parent entity 
	 * 
	 * @param name - value of name of created PersonNode
	 * @param parentNode - value of parent GroupNode for created PersonNode
	 * @param address - value of address of created PersonNode
	 * @param city - value of city of created PersonNode
	 * @param result - value of result of created PersonNode
	 * @param file - value of file path (or URI, or else) to picture of created PersonNode
	 * @return
	 */
	public static PersonNode createPersonNode(
			String name, GroupNode parentNode, String address, String city, Integer result, String file) {
		
		PersonNode childPerson = new PersonNode(name, parentNode, address, city, result, file);
		
		if (!name.isEmpty()) {
			parentNode.addChild(childPerson);
		}
		return  childPerson;
	}
	
	/**
	 * Method creating a blank PersonNode (all fields except for parent are empty). 
	 * 
	 * @param parentNode - node to be set as Parent
	 * @return - created PersonNode
	 */
	public static PersonNode createBlankPerson(GroupNode parentNode) {
		return createPersonNode("", parentNode, "", "", -1, "");
	}
	
	/**
	 * Method returns all nodes that are below current node in the hierarchy
	 * (childs, childs of childs etc..)
	 * 
	 * @param baseNode - current node
	 * @return - list of all nodes that are below in hierarchy
	 */
	public static List<BaseNode> getAllChilds(BaseNode baseNode){
		return getChilds(baseNode, new ArrayList<BaseNode>(List.of(baseNode)));
	}
	
	/**
	 * Method finds a node in tree by node's id
	 * 
	 * @param root - root element of tree
	 * @param id - id of node to be found
	 * @return - node with given id
	 */
	public static BaseNode getNodeById(BaseNode root, String id) {
		return getAllChilds(root).stream()
				.filter(node -> node.getId().equals(id))
				.findFirst().get();
	}

	/** 
	 * Method checks weather the child node is one of the parentNode's childs 
	 * (can be child, grandchild etc)
	 * 
	 * @param parentNode - node whose childs will be inspected
	 * @param childNode - expected child
	 * @return true if the child node is one of the parentNode's childs. Otherwise returns false
	 */
	public static boolean verifyRelationship(GroupNode parentNode, BaseNode childNode) {
		while (childNode.getParent() != null) {

			if (verifyParent(parentNode, childNode).isPresent()) {
				return true;
			}
			
			childNode = childNode.getParent();
		}

		return false;
	}
	
	private static List<BaseNode> getChilds(BaseNode baseNode, List<BaseNode> resultList) {
		if (baseNode instanceof GroupNode) {
			
			List<BaseNode> childs = new ArrayList<>(Arrays.asList(((GroupNode)baseNode).getEntries())); 
			resultList.addAll(childs);
			
			for (BaseNode childNode : childs) {
				getChilds(childNode, resultList);
			}
		}
		return resultList;
	}
}
